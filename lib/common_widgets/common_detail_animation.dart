import 'package:flutter/material.dart';
import 'package:animations/animations.dart';
import 'package:retalk/theme/colors.dart';

class CommonDetailAnimation extends StatelessWidget {
  final Widget detail;
  final Widget child;
  final Color color;
  const CommonDetailAnimation({
    Key? key,
    required this.detail,
    required this.child,
    this.color = backgroundColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return OpenContainer(
      closedColor: color,
      closedElevation: 0,
      transitionType: ContainerTransitionType.fade,
      openBuilder: (BuildContext context, VoidCallback _) {
        return detail;
      },
      closedBuilder: (BuildContext context, VoidCallback _) {
        return child;
      },
    );
  }
}
