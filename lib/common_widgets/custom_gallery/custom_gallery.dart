import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:iconly/iconly.dart';
import 'package:photo_manager/photo_manager.dart';
import 'package:retalk/common_widgets/custom_gallery/gallery_utils.dart';
import 'package:retalk/common_widgets/custom_gallery/grid_multi_pick.dart';
import 'package:retalk/common_widgets/custom_gallery/grid_single_pick.dart';
import 'package:retalk/common_widgets/custom_gallery/grid_video_pick.dart';
import 'package:retalk/get_it_routing/jump.dart';
import 'package:retalk/get_it_routing/locator.dart';
import 'package:retalk/theme/colors.dart';
import 'package:universal_io/io.dart';

class CustomGallery extends StatefulWidget {
  final bool? enableMulti;
  final bool? enableVideo;

  const CustomGallery({
    Key? key,
    this.enableMulti,
    this.enableVideo,
  }) : super(key: key);

  @override
  _CustomGalleryState createState() => _CustomGalleryState();
}

class _CustomGalleryState extends State<CustomGallery> {
  List<Widget> mediaList = [];
  List<AssetPathEntity> albums = [];
  List<Uint8List?> thumb = [];
  List<AssetEntity> unSelectedData = [];
  List<AssetEntity> selectedData = [];
  List<List<int>> selectedIndex = [];
  List<int?> single = [];
  int currentPage = 0;
  int selectedAlbum = 0;
  int? selectedImage;
  int? lastPage;
  bool? enableMulti;
  bool? enableVideo;
  @override
  void initState() {
    super.initState();
    enableMulti = widget.enableMulti ?? false;
    enableVideo = widget.enableVideo ?? false;
    fetchAlbum().then((_) {
      _fetchNewMedia();
    });
  }

  _handleScrollEvent(ScrollNotification scroll) {
    if (scroll.metrics.pixels / scroll.metrics.maxScrollExtent > 0.33) {
      if (currentPage != lastPage) {
        _fetchNewMedia();
      }
    }
  }

  Future<void> fetchAlbum() async {
    albums = await PhotoManager.getAssetPathList(onlyAll: false);
    for (int i = 0; i < albums.length; i++) {
      await albums[i].getAssetListPaged(0, 1).then((value) async {
        if (value.isNotEmpty) {
          Uint8List? file = await value[0].thumbDataWithSize(
            200,
            200,
            quality: 50,
          );
          selectedIndex.add([]);
          single.add(null);
          thumb.add(file);
        }
      });
    }
  }

  _fetchNewMedia() async {
    List<Widget> temp = [];
    List<AssetEntity> tempData = [];
    lastPage = currentPage;

    List<AssetEntity> media = await albums[selectedAlbum].getAssetListPaged(
      currentPage,
      60,
    );

    if (!enableVideo!) {
      for (var asset in media) {
        if (asset.type != AssetType.video) {
          tempData.add(asset);
          temp.add(
            FutureBuilder<Uint8List?>(
              future: asset.thumbDataWithSize(200, 200, quality: 50),
              builder: (BuildContext context, snapshot) {
                if (snapshot.hasData) {
                  return Image.memory(
                    snapshot.data!,
                    fit: BoxFit.cover,
                  );
                } else {
                  return Container();
                }
              },
            ),
          );
        }
      }
    } else {
      for (var asset in media) {
        if (asset.type != AssetType.image) {
          tempData.add(asset);
          temp.add(
            FutureBuilder<Uint8List?>(
              future: asset.thumbDataWithSize(200, 200, quality: 50),
              builder: (BuildContext context, snapshot) {
                if (snapshot.hasData) {
                  return Image.memory(
                    snapshot.data!,
                    fit: BoxFit.cover,
                  );
                } else {
                  return Container();
                }
              },
            ),
          );
        }
      }
    }

    setState(() {
      mediaList.addAll(temp);
      unSelectedData.addAll(tempData);
      currentPage++;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 3,
        backgroundColor: mainColor,
        titleSpacing: 5,
        automaticallyImplyLeading: false,
        title: Stack(
          children: <Widget>[
            Row(
              children: [
                GestureDetector(
                  onTap: () => Jump.back(),
                  child: Container(
                    margin: const EdgeInsets.only(right: 15),
                    height: 40,
                    width: 40,
                    child: const Center(
                      child: Icon(IconlyLight.arrow_left),
                    ),
                  ),
                ),
                AnimatedSwitcher(
                  duration: const Duration(milliseconds: 300),
                  transitionBuilder: (a, b) => ScaleTransition(
                    child: a,
                    scale: b,
                  ),
                  child: (albums.isNotEmpty)
                      ? GestureDetector(
                          onTap: () => onTapAlbum(),
                          child: Row(
                            children: [
                              Text(
                                albums[selectedAlbum].name,
                                style: const TextStyle(
                                  color: backgroundColor,
                                  fontWeight: FontWeight.w300,
                                  fontSize: 20,
                                ),
                              ),
                              const RotatedBox(
                                quarterTurns: 1,
                                child: Padding(
                                  padding: EdgeInsets.only(top: 3.0),
                                  child: Icon(
                                    Icons.chevron_right,
                                    color: backgroundColor,
                                    size: 30,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        )
                      : const SizedBox(
                          height: 40,
                          width: 40,
                          child: Center(
                            child: CupertinoActivityIndicator(),
                          ),
                        ),
                )
              ],
            ),
            SizedBox(
              height: 45,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  GestureDetector(
                    behavior: HitTestBehavior.opaque,
                    onTap: () async {
                      if (enableMulti!) {
                        List<File> result = [];
                        for (int i = 0; i < selectedData.length; i++) {
                          File? data = await selectedData[i].file;
                          result.add(data!);
                        }
                        locator<NavigatorService>().goBack(result);
                      } else {
                        File? file = await selectedData[0].file;
                        File? result = await compressFile(file!);
                        // ignore: unused_local_variable
                        ImageClass data = determineRatio(
                          selectedData[0].width,
                          selectedData[0].height,
                          result!,
                        );
                        // locator<NavigatorService>().goBack(data);
                        Navigator.of(context).pop(result);
                      }
                    },
                    child: Center(
                      child: Container(
                        padding: const EdgeInsets.only(right: 20, left: 10),
                        child: const Text(
                          'Done',
                          style: TextStyle(
                            fontSize: 20,
                            fontFamily: 'AvertaPE',
                            fontWeight: FontWeight.w300,
                            color: backgroundColor,
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
      body: NotificationListener<ScrollNotification>(
        onNotification: (ScrollNotification scroll) {
          _handleScrollEvent(scroll);
          return false;
        },
        child: (mediaList.isNotEmpty)
            ? returnGallery()
            : Center(
                child: Text(
                  (!enableVideo!)
                      ? 'There are no image file on this album'
                      : 'There are no video file on this album',
                ),
              ),
      ),
    );
  }

  onTapAlbum() {
    showAlbum(
      albums,
      thumb,
      context,
      selected: selectedIndex,
    ).then(
      (int? val) {
        if (val != null) {
          selectedAlbum = val;
          currentPage = 0;
          mediaList.clear();
          unSelectedData.clear();
          _fetchNewMedia();
          setState(() {});
        }
      },
    );
  }

  Widget returnGallery() {
    if (enableMulti!) {
      return gridMultiPick();
    } else if (enableVideo!) {
      return gridVideoPick();
    } else {
      return gridSinglePick();
    }
  }

  Widget gridVideoPick() => GridView.builder(
        physics: const BouncingScrollPhysics(),
        itemCount: mediaList.length,
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          crossAxisSpacing: 4,
          mainAxisSpacing: 4,
        ),
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            onTap: () {
              setState(() {
                if (single[selectedAlbum] == index) {
                  single[selectedAlbum] = null;
                  selectedData.remove(unSelectedData[index]);
                } else {
                  if (single[selectedAlbum] != null) {
                    int x = single[selectedAlbum]!;
                    single[selectedAlbum] = index;
                    selectedData.add(unSelectedData[index]);
                    selectedData.remove(unSelectedData[x]);
                  } else {
                    single[selectedAlbum] = index;
                    selectedData.add(unSelectedData[index]);
                  }
                }
              });
            },
            child: GridVideoPick(
              child: mediaList[index],
              isSelected: singleCheck(index),
              duration: formatDuration(
                  Duration(seconds: unSelectedData[index].duration)),
            ),
          );
        },
      );

  Widget gridMultiPick() => GridView.builder(
        physics: const BouncingScrollPhysics(),
        itemCount: mediaList.length,
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          crossAxisSpacing: 4,
          mainAxisSpacing: 4,
        ),
        itemBuilder: (BuildContext context, int index) {
          return GridItem(
            key: Key((index + 1).toString()),
            child: mediaList[index],
            check: checking(index),
            isSelected: (res) {
              setState(() {
                if (res) {
                  selectedData.add(unSelectedData[index]);
                  selectedIndex[selectedAlbum].add(index);
                } else {
                  selectedData.remove(unSelectedData[index]);
                  selectedIndex[selectedAlbum].remove(index);
                }
              });
            },
          );
        },
      );

  Widget gridSinglePick() => GridView.builder(
        physics: const BouncingScrollPhysics(),
        itemCount: mediaList.length,
        gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 3,
          crossAxisSpacing: 4,
          mainAxisSpacing: 4,
        ),
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            onTap: () {
              setState(() {
                if (single[selectedAlbum] == index) {
                  single[selectedAlbum] = null;
                  selectedData.remove(unSelectedData[index]);
                } else {
                  if (single[selectedAlbum] != null) {
                    int x = single[selectedAlbum]!;
                    single[selectedAlbum] = index;
                    selectedData.add(unSelectedData[index]);
                    selectedData.remove(unSelectedData[x]);
                  } else {
                    single[selectedAlbum] = index;
                    selectedData.add(unSelectedData[index]);
                  }
                }
              });
            },
            child: GridSinglePick(
              child: mediaList[index],
              isSelected: singleCheck(index),
            ),
          );
        },
      );

  Future<int> showAlbum(
    List<AssetPathEntity> album,
    List<Uint8List?> image,
    BuildContext context, {
    List<int>? singleSelect,
    List<List<int>>? selected,
  }) async {
    return showModalBottomSheet(
      context: context,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(top: Radius.circular(20)),
      ),
      // isScrollControlled: true,
      builder: (context) {
        return Container(
          padding: const EdgeInsets.only(top: 50, bottom: 30),
          child: ListView.separated(
            separatorBuilder: (context, index) {
              return const Divider();
            },
            itemCount: album.length,
            itemBuilder: (context, index) {
              return ListTile(
                title: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(album[index].name),
                    const SizedBox(height: 15),
                    Text(album[index].assetCount.toString()),
                  ],
                ),
                trailing: Text(
                  selected![index].isNotEmpty
                      ? selected[index].length.toString()
                      : '',
                ),
                leading: SizedBox(
                  height: 60,
                  width: 60,
                  child: Image.memory(
                    image[index]!,
                    fit: BoxFit.cover,
                  ),
                ),
                onTap: () {
                  locator<NavigatorService>().goBack(index);
                },
              );
            },
          ),
        );
      },
    ).then((res) {
      return res;
    });
  }

  bool checking(int i) {
    return selectedIndex[selectedAlbum].isNotEmpty &&
        selectedIndex[selectedAlbum].contains(i);
  }

  bool singleCheck(int i) {
    return single[selectedAlbum] != null && single[selectedAlbum] == i;
  }
}
