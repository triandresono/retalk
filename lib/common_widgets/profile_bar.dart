import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:retalk/common_widgets/common_avatar.dart';
import 'package:retalk/get_it_routing/jump.dart';
import 'package:retalk/helper/page_key.dart';
import 'package:retalk/models/user_profile_menu.dart';
import 'package:retalk/models/users.dart';
import 'package:retalk/screens/profile_page/user_profile_icon_menu.dart';
import 'package:retalk/state_management/auth_bloc/auth_bloc.dart';
import 'package:retalk/state_management/auth_bloc/auth_event.dart';
import 'package:retalk/state_management/auth_bloc/auth_state.dart';
import 'package:retalk/theme/app_theme.dart';
import 'package:retalk/theme/colors.dart';

class ProfileBar extends StatefulWidget {
  final double opacity;
  final Users user;
  final Function() changeImage;
  final bool isUser;
  const ProfileBar({
    Key? key,
    this.opacity = 0,
    this.isUser = true,
    required this.user,
    required this.changeImage,
  }) : super(key: key);

  @override
  _ProfileBarState createState() => _ProfileBarState();
}

class _ProfileBarState extends State<ProfileBar> {
  bloc(AuthEvent event) {
    BlocProvider.of<AuthBloc>(context).add(event);
  }

  @override
  Widget build(BuildContext context) {
    return blocListener(
      child: AppBar(
        elevation: 0,
        backgroundColor: mainColor.withOpacity(widget.opacity),
        title: Visibility(
          visible: widget.opacity > 0.4,
          child: Row(
            children: [
              CommonAvatar(
                radius: 20,
                hash: widget.user.userImageHash ?? '',
                imageUrl: widget.user.userImage ?? '',
              ),
              const SizedBox(width: 10),
              Text(
                widget.user.userName ?? '',
                overflow: TextOverflow.ellipsis,
                style: MyTheme.chatSenderName,
              ),
            ],
          ),
        ),
        actions: [
          Visibility(
            visible: widget.isUser,
            child: ClipOval(
              child: BackdropFilter(
                filter: ImageFilter.blur(
                  sigmaX: 3.0,
                  sigmaY: 3.0,
                ),
                child: UserProfileIconMenu(
                  onSelected: (val) => onSelect(val),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  onSelect(val) {
    if (val == ProfileMenu.logout) {
      bloc(AuthLogout());
    } else {
      widget.changeImage();
    }
  }

  blocListener({required Widget child}) {
    return BlocListener<AuthBloc, AuthState>(
      listener: (context, state) {
        if (state is LogoutDone) {
          Jump.replace(Pages.loginMain);
        }
      },
      child: child,
    );
  }
}
