import 'package:flutter/material.dart';
import 'package:retalk/theme/colors.dart';

class CommonProfileText extends StatelessWidget {
  final String text;
  const CommonProfileText({Key? key, required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: 15),
      child: Text(
        text,
        style: const TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.w200,
          color: mainColor,
        ),
      ),
    );
  }
}
