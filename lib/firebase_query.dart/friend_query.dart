import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:retalk/firebase_query.dart/query_key.dart';
import 'package:retalk/models/users.dart';

class FriendQuery {
  static getFriendList(map) async {
    return FirebaseFirestore.instance
        .collection(Col.friends)
        .doc(map[UserKey.userId])
        .collection(Col.friendList)
        .where(UserKey.userSearch, arrayContains: map['val'])
        .snapshots();
  }

  static addFriends({required friend, required user}) {
    friend[UserKey.userSearch] = generateStringKey(friend[UserKey.userName]);
    user[UserKey.userSearch] = generateStringKey(user[UserKey.userName]);
    FirebaseFirestore.instance
        .collection(Col.friends)
        .doc(user[UserKey.userId])
        .collection(Col.friendList)
        .doc(friend[UserKey.userId])
        .set(friend)
        .catchError((e) => throw e);

    FirebaseFirestore.instance
        .collection(Col.friends)
        .doc(friend[UserKey.userId])
        .collection(Col.friendList)
        .doc(user[UserKey.userId])
        .set(user)
        .catchError((e) => throw e);
  }

  static updateUserData(Users user) async {
    List<String> friendId = [];

    await FirebaseFirestore.instance
        .collection(Col.friends)
        .doc(user.userId)
        .collection(Col.friendList)
        .get()
        .then((value) {
      for (var element in value.docs) {
        friendId.add(element.data()[UserKey.userId]);
      }
    }).catchError((e) => throw e);

    if (friendId.isNotEmpty) {
      for (int i = 0; i < friendId.length; i++) {
        //update friend
        await FirebaseFirestore.instance
            .collection(Col.friends)
            .doc(friendId[i])
            .collection(Col.friendList)
            .doc(user.userId)
            .update(user.toUpdateFriend())
            .catchError((e) => throw e);

        //update chat peers
        await FirebaseFirestore.instance
            .collection(Col.chats)
            .doc(friendId[i])
            .collection(Col.allChats)
            .doc(user.userId)
            .update(user.tpUpdateLastChat())
            .catchError((e) => throw e);
        //update users
        await FirebaseFirestore.instance
            .collection(Col.users)
            .doc(user.userId)
            .update(user.toUpdateUser())
            .catchError((e) => throw e);
      }
    } else {
      await FirebaseFirestore.instance
          .collection(Col.users)
          .doc(user.userId)
          .update(user.toUpdateUser())
          .catchError((e) => throw e);
    }
  }
}
