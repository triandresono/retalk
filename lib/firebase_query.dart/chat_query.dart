import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:retalk/firebase_query.dart/query_key.dart';
import 'package:retalk/models/chat_data.dart';

class ChatQuery {
  static getRoomList(String id) async {
    List<String> roomId = [];
    QuerySnapshot snap = await FirebaseFirestore.instance
        .collection(Col.chatRoom)
        .where(RoomKey.users, arrayContains: id)
        .get()
        .catchError((e) => throw e);

    for (var element in snap.docs) {
      roomId.add(element[RoomKey.roomId]);
    }
    return roomId;
  }

  static createChatRoom(map) {
    FirebaseFirestore.instance
        .collection(Col.chatRoom)
        .doc(map[RoomKey.roomId])
        .set(map)
        .catchError((e) => throw e);
  }

  static createInRoomStatus(map) {
    Map<String, dynamic> user = {RoomKey.isInRoom: true};
    Map<String, dynamic> friend = {RoomKey.isInRoom: false};

    FirebaseFirestore.instance
        .collection(Col.chatRoom)
        .doc(map[RoomKey.roomId])
        .collection(Col.isInRoom)
        .doc(map[UserKey.userId])
        .set(user)
        .catchError((e) => throw e);

    FirebaseFirestore.instance
        .collection(Col.chatRoom)
        .doc(map[RoomKey.roomId])
        .collection(Col.isInRoom)
        .doc(map[UserKey.friendId])
        .set(friend)
        .catchError((e) => throw e);
  }

  static updateInRoom(map) {
    Map<String, dynamic> data = {RoomKey.isInRoom: map[RoomKey.isInRoom]};

    FirebaseFirestore.instance
        .collection(Col.chatRoom)
        .doc(map[RoomKey.roomId])
        .collection(Col.isInRoom)
        .doc(map[UserKey.userId])
        .set(data)
        .catchError((e) => throw e);
  }

  static updateIsRead(map) async {
    var update = {RoomKey.isRead: true};
    CollectionReference snap = FirebaseFirestore.instance
        .collection(Col.chatRoom)
        .doc(map[RoomKey.roomId])
        .collection(Col.chatData);

    QuerySnapshot eventsQuery = await snap
        .where(RoomKey.isRead, isEqualTo: false)
        .where(RoomKey.sendBy, isEqualTo: map[UserKey.userName])
        .get()
        .catchError((e) => throw e);

    if (eventsQuery.docs.isNotEmpty) {
      for (var msgDoc in eventsQuery.docs) {
        msgDoc.reference.update(update).catchError((e) => throw e);
      }
    }
  }

  static updateLastChat(map) {
    Map<String, dynamic> lastChat = {};
    lastChat[UserKey.lastChat] = map[UserKey.lastChat];
    lastChat[UserKey.lastChatType] = map[UserKey.lastChatType];

    FirebaseFirestore.instance
        .collection(Col.chatRoom)
        .doc(map[RoomKey.roomId])
        .update(lastChat)
        .catchError((e) => throw e);
  }

  static createChats({required user, required friend}) {
    FirebaseFirestore.instance
        .collection(Col.chats)
        .doc(user[UserKey.userId])
        .collection(Col.allChats)
        .doc(friend[UserKey.userId])
        .set(friend)
        .catchError((e) => throw e);

    FirebaseFirestore.instance
        .collection(Col.chats)
        .doc(friend[UserKey.userId])
        .collection(Col.allChats)
        .doc(user[UserKey.userId])
        .set(user)
        .catchError((e) => throw e);
  }

  static searchChat({required user, required String val}) async {
    Stream<QuerySnapshot> stream;
    bool isEmpty = await checkChats(user: user, val: val);

    try {
      if (isEmpty) {
        stream = FirebaseFirestore.instance
            .collection(Col.chats)
            .doc(user[UserKey.userId])
            .collection(Col.allChats)
            .where(UserKey.lastSearchChat, arrayContains: val)
            .snapshots();
      } else {
        stream = FirebaseFirestore.instance
            .collection(Col.chats)
            .doc(user[UserKey.userId])
            .collection(Col.allChats)
            .where(UserKey.lastNameSearch, arrayContains: val)
            .snapshots();
      }
    } catch (e) {
      rethrow;
    }

    return stream;
  }

  static updateChatPeers(ChatData data) {
    Map<String, dynamic> result = {};
    result[UserKey.lastChat] = data.message;
    result[UserKey.lastChatType] = data.chatType;
    result[UserKey.lastImage] = data.imageUrl;

    FirebaseFirestore.instance
        .collection(Col.chats)
        .doc(data.userId)
        .collection(Col.allChats)
        .doc(data.friendId)
        .update(result)
        .catchError((e) => throw e);

    FirebaseFirestore.instance
        .collection(Col.chats)
        .doc(data.friendId)
        .collection(Col.allChats)
        .doc(data.userId)
        .update(result)
        .catchError((e) => throw e);
  }

  static addConversationMessage(ChatData data) {
    FirebaseFirestore.instance
        .collection(Col.chatRoom)
        .doc(data.roomId)
        .collection(Col.chatData)
        .add(data.toMap())
        .catchError((e) => throw e);
  }

  static getConversationMessage(String chatroomId) async {
    return FirebaseFirestore.instance
        .collection(Col.chatRoom)
        .doc(chatroomId)
        .collection(Col.chatData)
        .orderBy(RoomKey.chatTime, descending: true)
        .snapshots();
  }

  static getInRoomStatus(map) {
    return FirebaseFirestore.instance
        .collection(Col.chatRoom)
        .doc(map[RoomKey.roomId])
        .collection(Col.isInRoom)
        .doc(map[UserKey.userId])
        .snapshots();
  }

  static checkChats({required user, required String val}) async {
    Stream<QuerySnapshot> stream = FirebaseFirestore.instance
        .collection(Col.chats)
        .doc(user[UserKey.userId])
        .collection(Col.allChats)
        .where(UserKey.lastNameSearch, arrayContains: val)
        .snapshots();

    return await stream.isEmpty;
  }
}
