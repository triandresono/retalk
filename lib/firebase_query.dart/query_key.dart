class UserKey {
  static const userId = 'userId';
  static const userName = 'userName';
  static const userMail = 'userMail';
  static const userProfile = 'userProfile';
  static const userImage = 'userImage';
  static const userImageHash = 'userImageHash';
  static const userSearch = 'userSearch';
  static const onlineStatus = 'onlineStatus';

  //chat
  static const friendId = 'friendId';
  static const lastChat = 'lastChat';
  static const lastImage = 'lastImage';
  static const lastChatType = 'lastChatType';
  static const lastNameSearch = 'lastNameSearch';
  static const lastSearchChat = 'lastSearchChat';
}

class RoomKey {
  static const roomId = 'roomId';
  static const users = 'users';
  static const message = 'message';
  static const sendBy = 'sendBy';
  static const isRead = 'isRead';
  static const hash = 'hash';
  static const chatType = 'chatType';
  static const chatTime = 'chatTime';
  static const isInRoom = 'isInRoom';
  static const imageUrl = 'imageUrl';
}

class Col {
  static const users = 'Users';
  static const onlineStatus = 'OnlineStatus';
  static const friends = 'Friends';
  static const chats = 'Chats';
  static const chatRoom = 'ChatRoom';
  static const friendList = 'FriendList';
  static const allChats = 'AllChats';
  static const chatData = 'ChatData';
  static const isInRoom = 'IsInRoom';
}

getChatRoomId(String? a, String? b) {
  if (a!.substring(0, 1).codeUnitAt(0) > b!.substring(0, 1).codeUnitAt(0)) {
    return b + "_" + a;
  } else {
    return a + "_" + b;
  }
}

List<String> generateStringKey(String? data) {
  List<String> caseSearchList = [''];
  String temp = "";
  for (int i = 0; i < data!.length; i++) {
    temp = (temp + data[i]).toLowerCase();
    caseSearchList.add(temp);
  }
  return caseSearchList;
}
