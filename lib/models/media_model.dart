class MediaModel {
  String? mediaName;
  String? imageUrl;
  String? mediaContent;
  String? blurHash;

  MediaModel({
    this.imageUrl,
    this.mediaContent,
    this.blurHash,
    this.mediaName,
  });
}
