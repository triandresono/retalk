import 'package:universal_io/io.dart';

class Users {
  String? userId;
  String? userName;
  String? userMail;
  String? userImage;
  String? userImageHash;
  String? userProfile;

  //helper
  String? password;
  File? file;
  //chat
  String? lastChat;
  String? lastImage;
  String? lastChatType;
  List<String>? lastNameSearch;
  List<String>? lastSearchChat;
  //friend
  List<String>? userSearch;

  Users({
    this.userId,
    this.userName,
    this.userMail,
    this.userImage,
    this.userImageHash,
    this.userProfile,
    //helper
    this.password,
    this.file,
    //chat
    this.lastNameSearch,
    this.lastSearchChat,
    this.lastChat,
    this.lastChatType,
    this.lastImage,
    //friend
    this.userSearch,
  });

  Map<String, dynamic> toUpdateUser() {
    Map<String, dynamic> map = {};
    map["userName"] = userName;
    map["userImage"] = userImage;
    map["userImageHash"] = userImageHash;
    map["userProfile"] = userProfile;
    return map;
  }

  Map<String, dynamic> toUpdateFriend() {
    Map<String, dynamic> map = {};
    map["userName"] = userName;
    map["userImage"] = userImage;
    map["userImageHash"] = userImageHash;
    map["userProfile"] = userProfile;
    map["userSearch"] = userSearch;
    return map;
  }

  Map<String, dynamic> tpUpdateLastChat() {
    Map<String, dynamic> map = {};
    map["userName"] = userName;
    map["userImage"] = userImage;
    map["userImageHash"] = userImageHash;
    map["userProfile"] = userProfile;
    map["lastNameSearch"] = lastNameSearch;
    return map;
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {};
    map["userId"] = userId;
    map["userName"] = userName;
    map["userMail"] = userMail;
    map["userImage"] = userImage;
    map["userImageHash"] = userImageHash;
    map["userProfile"] = userProfile;
    return map;
  }

  Map<String, dynamic> toChat() {
    Map<String, dynamic> map = {};
    map["userId"] = userId;
    map["userName"] = userName;
    map["userImage"] = userImage;
    map["userImageHash"] = userImageHash;
    map["userProfile"] = userProfile;
    map["lastChat"] = lastChat;
    map["lastImage"] = lastImage;
    map["lastChatType"] = lastChatType;
    map["lastNameSearch"] = lastNameSearch;
    map["lastSearchChat"] = lastSearchChat;
    return map;
  }

  Users.map(Map<String, dynamic> json) {
    userId = json['userId'];
    userName = json['userName'];
    userMail = json['userMail'];
    userImage = json['userImage'];
    userImageHash = json['userImageHash'];
    userProfile = json['userProfile'];
  }

  Users.chat(Map<String, dynamic> json) {
    userId = json['userId'];
    userName = json['userName'];
    userMail = json['userMail'];
    userImage = json['userImage'];
    userImageHash = json['userImageHash'];
    userProfile = json['userProfile'];
    lastChat = json['lastChat'];
    lastImage = json['lastImage'];
    lastChatType = json['lastChatType'];
  }
}
