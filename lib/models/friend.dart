class Friend {
  String? userId;
  String? userName;
  String? userMail;
  String? userImage;
  String? userImageHash;
  String? userProfile;
  List<String>? userSearch;

  Friend({
    this.userId,
    this.userName,
    this.userMail,
    this.userImage,
    this.userImageHash,
    this.userProfile,
    this.userSearch,
  });

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {};
    map["userId"] = userId;
    map["userName"] = userName;
    map["userMail"] = userMail;
    map["userImage"] = userImage;
    map["userImageHash"] = userImageHash;
    map["userProfile"] = userProfile;
    return map;
  }

  Friend.fromJson(Map<String, dynamic> json) {
    userId = json['userId'];
    userName = json['userName'];
    userMail = json['userMail'];
    userImage = json['userImage'];
    userImageHash = json['userImageHash'];
    userProfile = json['userProfile'];
    userSearch = json['userSearch'].cast<String>();
  }
}
