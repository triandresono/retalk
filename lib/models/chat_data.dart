class ChatData {
  String? message;
  String? imageUrl;
  String? chatType;
  String? hash;
  String? sendBy;
  bool? isRead;
  int? chatTime;

  //helper
  String? roomId;
  String? friendId;
  String? userId;
  String? friendProfileImage;
  String? friendProfileHash;

  ChatData({
    this.message,
    this.chatType,
    this.hash,
    this.sendBy,
    this.isRead,
    this.chatTime,
    this.imageUrl,
    //helper
    this.roomId,
    this.friendId,
    this.userId,
    this.friendProfileImage,
    this.friendProfileHash,
  });

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = {};
    map["message"] = message;
    map["chatType"] = chatType;
    map["hash"] = hash;
    map["sendBy"] = sendBy;
    map["isRead"] = isRead;
    map["chatTime"] = chatTime;
    map["imageUrl"] = imageUrl;
    return map;
  }

  ChatData.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    chatType = json['chatType'];
    hash = json['hash'];
    sendBy = json['sendBy'];
    isRead = json['isRead'];
    chatTime = json['chatTime'];
    imageUrl = json['imageUrl'];
  }
}
