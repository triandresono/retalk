class Pages {
  static const loginMain = 'loginMain';
  static const homePage = 'homePage';
  static const friendPage = 'friendPage';
  static const chatRoomPage = 'chatRoomPage';
  static const userProfile = 'userProfile';
  static const friendProfile = 'friendProfile';
}
