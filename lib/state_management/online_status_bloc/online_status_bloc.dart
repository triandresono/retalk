import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:retalk/firebase_query.dart/login_query.dart';
import 'package:retalk/models/users.dart';
import 'package:retalk/service.dart/database.dart';
import 'package:retalk/state_management/online_status_bloc/online_status_event.dart';
import 'package:retalk/state_management/online_status_bloc/online_status_state.dart';

class OnlineStatusBloc extends Bloc<OnlineStatusEvent, OnlineStatusState> {
  Users user = Users();
  Stream<DocumentSnapshot>? onelineStats;
  OnlineStatusBloc() : super(InitialOnlineStatus()) {
    on<OnlineStatusEvent>((event, emit) => start(event, emit));
  }

  start(OnlineStatusEvent event, Emitter<OnlineStatusState> emit) async {
    try {
      user = await DB.getUser();
      if (event is InitOnline) {
        await initOnline(event.userId);
        emit(GetStatusDone(onelineStats!));
      }
    } catch (e) {
      emit(GetStatusFailed(e.toString()));
    }
  }

  initOnline(String userId) async {
    onelineStats = await LogQuery.getOnlineStatus(userId);
  }
}
