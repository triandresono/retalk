class OnlineStatusEvent {}

class InitOnline extends OnlineStatusEvent {
  final String userId;

  InitOnline(this.userId);
}
