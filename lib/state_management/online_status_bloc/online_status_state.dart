import 'package:cloud_firestore/cloud_firestore.dart';

class OnlineStatusState {}

class InitialOnlineStatus extends OnlineStatusState {}

class GetStatusDone extends OnlineStatusState {
  final Stream<DocumentSnapshot> roomStat;

  GetStatusDone(this.roomStat);
}

class GetStatusFailed extends OnlineStatusState {
  final String error;

  GetStatusFailed(this.error);
}
