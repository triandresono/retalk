import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:retalk/firebase_query.dart/chat_query.dart';
import 'package:retalk/firebase_query.dart/query_key.dart';
import 'package:retalk/models/users.dart';
import 'package:retalk/service.dart/database.dart';
import 'package:retalk/state_management/chat_page_bloc/chat_page_event.dart';
import 'package:retalk/state_management/chat_page_bloc/chat_page_state.dart';

class ChatPageBloc extends Bloc<ChatPageEvent, ChatPageState> {
  Users user = Users();
  Map<String, dynamic> data = {};
  ChatPageBloc() : super(InitialChatPage()) {
    on<ChatPageEvent>((event, emit) => start(event, emit));
  }

  start(ChatPageEvent event, Emitter<ChatPageState> emit) async {
    user = await DB.getUser();
    try {
      if (event is GetChatPage) {
        emit(ChatPageOnLoading());
        var data = await getChat(event.val);
        emit(GetChatPageComplete(data));
      } else if (event is GotoChatRoom) {
        emit(ChatPageOnLoading());
        await inRoom(event.map);
        emit(GoChatRoomComplete(data));
      }
    } catch (e) {
      emit(ChatPageFailed(e.toString()));
    }
  }

  getChat(String val) async {
    return await ChatQuery.searchChat(user: user.toMap(), val: val);
  }

  inRoom(Map<String, dynamic> map) async {
    Map<String, dynamic> newMap = {};
    String roomId = getChatRoomId(user.userId, map[UserKey.userId]);
    newMap[RoomKey.isInRoom] = true;
    newMap[RoomKey.roomId] = roomId;
    newMap[UserKey.userId] = user.userId;
    data.addAll(map);
    data[RoomKey.roomId] = roomId;
    await ChatQuery.updateInRoom(newMap);
  }
}
