import 'package:cloud_firestore/cloud_firestore.dart';

class ChatPageState {}

class InitialChatPage extends ChatPageState {}

class ChatPageOnLoading extends ChatPageState {}

class GetChatPageComplete extends ChatPageState {
  final Stream<QuerySnapshot> stream;

  GetChatPageComplete(this.stream);
}

class GoChatRoomComplete extends ChatPageState {
  final Map<String, dynamic> result;

  GoChatRoomComplete(this.result);
}

class ChatPageFailed extends ChatPageState {
  final String message;

  ChatPageFailed(this.message);
}
