class ChatPageEvent {}

class GetChatPage extends ChatPageEvent {
  final String val;

  GetChatPage(this.val);
}

class GotoChatRoom extends ChatPageEvent {
  final Map<String, dynamic> map;

  GotoChatRoom(this.map);
}
