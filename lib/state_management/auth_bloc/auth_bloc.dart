import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:retalk/firebase_query.dart/login_query.dart';
import 'package:retalk/firebase_query.dart/query_key.dart';
import 'package:retalk/models/users.dart';
import 'package:retalk/service.dart/auth.dart';
import 'package:retalk/service.dart/database.dart';
import 'package:retalk/state_management/auth_bloc/auth_event.dart';
import 'package:retalk/state_management/auth_bloc/auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  Users user = Users();
  AuthBloc() : super(InitialAuth()) {
    on<AuthEvent>((event, emit) => start(event, emit));
  }

  start(AuthEvent event, Emitter<AuthState> emit) async {
    try {
      user = await DB.getUser();
      if (event is InitAuthEvent) {
        bool isLogin = await DB.checkLogin();
        if (isLogin) emit(IsLoggedIn());
      } else if (event is AuthLogout) {
        await doLogout();
        emit(LogoutDone());
      }
    } catch (e) {
      emit(AuthFailed(e.toString()));
    }
  }

  doLogout() async {
    Map<String, dynamic> map = user.toMap();
    map[UserKey.onlineStatus] = false;
    await LogQuery.updateOnline(map);
    await Auth.signOut();
    await DB.clear();
  }
}
