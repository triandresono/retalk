import 'package:retalk/models/users.dart';

class RegisterEvent {}

class RegisterSubmit extends RegisterEvent {
  final Users user;

  RegisterSubmit(this.user);
}
