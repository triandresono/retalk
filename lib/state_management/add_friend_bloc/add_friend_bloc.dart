import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:retalk/firebase_query.dart/chat_query.dart';
import 'package:retalk/firebase_query.dart/friend_query.dart';
import 'package:retalk/firebase_query.dart/login_query.dart';
import 'package:retalk/firebase_query.dart/query_key.dart';
import 'package:retalk/helper/utils.dart';
import 'package:retalk/models/users.dart';
import 'package:retalk/service.dart/database.dart';
import 'package:retalk/state_management/add_friend_bloc/add_friend_event.dart';
import 'package:retalk/state_management/add_friend_bloc/add_friend_state.dart';

class AddFriendBloc extends Bloc<AddFriendEvent, AddFriendState> {
  Users user = Users();
  List<DocumentSnapshot> result = [];
  String roomId = '';
  AddFriendBloc() : super(InitialAddFriend()) {
    on<AddFriendEvent>((event, emit) => start(event, emit));
  }

  start(AddFriendEvent event, Emitter<AddFriendState> emit) async {
    try {
      user = await DB.getUser();
      if (event is SearchFriendEvent) {
        emit(AddFriendProgress());
        await searchFriend(event.search);
        emit(SearchFriendDone(result));
      } else if (event is SetAddFriend) {
        emit(AddFriendProgress());
        await initingFriend(event.friend);
        emit(AddFriendComplete());
      }
    } catch (e) {
      emit(AddFriendFailed(e.toString()));
    }
  }

  searchFriend(String val) async {
    Map<String, dynamic> res = user.toMap();
    res['val'] = val;
    result = await LogQuery.friendSearch(res);
  }

  initingFriend(Map<String, dynamic> map) async {
    roomId = getChatRoomId(user.userId, map[UserKey.userId]);
    await addFriend(map);
    await createChats(map);
    await createChatRoom(map);
    await createInRoomStatus(map);
  }

  addFriend(Map<String, dynamic> map) async {
    await FriendQuery.addFriends(friend: map, user: user.toMap());
  }

  createChats(Map<String, dynamic> map) async {
    Users userData = userToChats(user.toMap());
    Users friendData = userToChats(map);

    await ChatQuery.createChats(
      user: userData.toChat(),
      friend: friendData.toChat(),
    );
  }

  createChatRoom(Map<String, dynamic> map) async {
    Map<String, dynamic> newMap = {};
    newMap[RoomKey.roomId] = roomId;
    newMap[RoomKey.users] = [user.userId, map[UserKey.userId]];
    await ChatQuery.createChatRoom(newMap);
  }

  createInRoomStatus(Map<String, dynamic> friend) async {
    Map<String, dynamic> map = {};
    map[UserKey.userId] = user.userId;
    map[UserKey.friendId] = friend[UserKey.userId];
    map[RoomKey.roomId] = roomId;
    await ChatQuery.createInRoomStatus(map);
  }
}
