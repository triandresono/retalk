class AddFriendEvent {}

class SearchFriendEvent extends AddFriendEvent {
  final String search;

  SearchFriendEvent(this.search);
}

class SetAddFriend extends AddFriendEvent {
  final Map<String, dynamic> friend;

  SetAddFriend(this.friend);
}
