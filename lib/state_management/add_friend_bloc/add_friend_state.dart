import 'package:cloud_firestore/cloud_firestore.dart';

class AddFriendState {}

class InitialAddFriend extends AddFriendState {}

class AddFriendProgress extends AddFriendState {}

class SearchFriendDone extends AddFriendState {
  final List<DocumentSnapshot> result;

  SearchFriendDone(this.result);
}

class AddFriendComplete extends AddFriendState {}

class AddFriendFailed extends AddFriendState {
  final String error;

  AddFriendFailed(this.error);
}
