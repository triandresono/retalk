class LoginEvent {}

class SubmitLogin extends LoginEvent {
  final Map<String, dynamic> data;

  SubmitLogin(this.data);
}
