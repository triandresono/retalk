class LoginState {}

class LoginInitialState extends LoginState {}

class LoginOnProgress extends LoginState {}

class LoginComplete extends LoginState {}

class LoginFailed extends LoginState {
  final String message;

  LoginFailed(this.message);
}
