import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:retalk/firebase_query.dart/chat_query.dart';
import 'package:retalk/firebase_query.dart/login_query.dart';
import 'package:retalk/firebase_query.dart/query_key.dart';
import 'package:retalk/helper/constant.dart';
import 'package:retalk/models/users.dart';
import 'package:retalk/service.dart/auth.dart';
import 'package:retalk/service.dart/database.dart';
import 'package:retalk/state_management/login_bloc/login_event.dart';
import 'package:retalk/state_management/login_bloc/login_state.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  LoginBloc() : super(LoginInitialState()) {
    on<LoginEvent>((event, emit) => start(event, emit));
  }

  start(LoginEvent event, Emitter<LoginState> emit) async {
    try {
      if (event is SubmitLogin) {
        emit(LoginOnProgress());
        await submitting(event.data);
        emit(LoginComplete());
      }
    } catch (e) {
      emit(LoginFailed(e.toString()));
    }
  }

  submitting(map) async {
    var id = await Auth.signInWithEmail(
      mail: map[Const.email],
      pass: map[Const.pass],
    );
    Users result = await LogQuery.getUsersById(id);
    await DB.saveUser(result);
    await getFriendList(id);
    await updateOnline(result);
  }

  getFriendList(String id) async {
    List<String>? roomIds = await ChatQuery.getRoomList(id);
    if (roomIds != null) await DB.initChatRoom(roomIds);
  }

  updateOnline(Users user) async {
    Map<String, dynamic> map = user.toMap();
    map[UserKey.onlineStatus] = true;
    await LogQuery.updateOnline(map);
  }
}
