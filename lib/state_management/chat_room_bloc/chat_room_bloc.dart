import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:retalk/firebase_query.dart/chat_query.dart';
import 'package:retalk/firebase_query.dart/query_key.dart';
import 'package:retalk/models/chat_data.dart';
import 'package:retalk/models/users.dart';
import 'package:retalk/service.dart/database.dart';
import 'package:retalk/state_management/chat_room_bloc/chat_room_event.dart';
import 'package:retalk/state_management/chat_room_bloc/chat_room_state.dart';

class ChatRoomBloc extends Bloc<ChatRoomEvent, ChatRoomState> {
  Users user = Users();
  Stream<QuerySnapshot>? chatStream;
  Stream<DocumentSnapshot>? roomStats;
  ChatRoomBloc() : super(InitialChatRoom()) {
    on<ChatRoomEvent>((event, emit) => start(event, emit));
  }

  start(ChatRoomEvent event, Emitter<ChatRoomState> emit) async {
    try {
      user = await DB.getUser();
      if (event is GetChatArgument) {
        emit(GetArgumentDone(user));
      } else if (event is InitChatRoom) {
        await initChat(event.map);
        emit(InitChatRoomDone(chatStream!, roomStats!));
      } else if (event is AddChatEvent) {
        await addChat(event.data);
        emit(AddChatComplete());
      }
    } catch (e) {
      emit(ChatRoomFailed(e.toString()));
    }
  }

  initChat(Map<String, dynamic> map) async {
    chatStream = await ChatQuery.getConversationMessage(map[RoomKey.roomId]);
    roomStats = await ChatQuery.getInRoomStatus(map);
    await ChatQuery.updateIsRead(map);
  }

  addChat(ChatData data) async {
    await ChatQuery.updateChatPeers(data);
    await ChatQuery.addConversationMessage(data);
  }
}
