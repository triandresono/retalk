import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:retalk/models/users.dart';

class ChatRoomState {}

class InitialChatRoom extends ChatRoomState {}

class GetArgumentDone extends ChatRoomState {
  final Users user;

  GetArgumentDone(this.user);
}

class InitChatRoomDone extends ChatRoomState {
  final Stream<QuerySnapshot> initChat;
  final Stream<DocumentSnapshot> roomStat;

  InitChatRoomDone(this.initChat, this.roomStat);
}

class AddChatComplete extends ChatRoomState {}

class ChatRoomFailed extends ChatRoomState {
  final String message;
  ChatRoomFailed(this.message);
}
