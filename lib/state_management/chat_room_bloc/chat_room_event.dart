import 'package:retalk/models/chat_data.dart';

class ChatRoomEvent {}

class GetChatArgument extends ChatRoomEvent {}

class InitChatRoom extends ChatRoomEvent {
  final Map<String, dynamic> map;

  InitChatRoom(this.map);
}

class AddChatEvent extends ChatRoomEvent {
  final ChatData data;

  AddChatEvent(this.data);
}
