import 'package:retalk/models/users.dart';

class UserProfileEvent {}

class InitUserProfile extends UserProfileEvent {}

class SubmitUpdateProfile extends UserProfileEvent {
  final Users user;
  SubmitUpdateProfile(this.user);
}
