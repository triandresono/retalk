import 'package:retalk/models/users.dart';

class UserProfileState {}

class InitialUserProfile extends UserProfileState {}

class UserProfileProgress extends UserProfileState {}

class ProfileUpdated extends UserProfileState {
  final Users newUser;

  ProfileUpdated(this.newUser);
}

class InitUserProfileDone extends UserProfileState {
  final Users user;

  InitUserProfileDone(this.user);
}

class UserProfileFailed extends UserProfileState {
  final String error;

  UserProfileFailed(this.error);
}
