import 'dart:typed_data';

import 'package:blurhash/blurhash.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:retalk/firebase_query.dart/friend_query.dart';
import 'package:retalk/firebase_query.dart/login_query.dart';
import 'package:retalk/firebase_query.dart/query_key.dart';
import 'package:retalk/models/users.dart';
import 'package:retalk/service.dart/database.dart';
import 'package:retalk/state_management/user_profile_bloc/user_profile_event.dart';
import 'package:retalk/state_management/user_profile_bloc/user_profile_state.dart';
import 'package:universal_io/io.dart';
import 'package:path/path.dart';

class UserProfileBloc extends Bloc<UserProfileEvent, UserProfileState> {
  Users users = Users();
  String? url;
  String? hash;
  UserProfileBloc() : super(InitialUserProfile()) {
    on<UserProfileEvent>((event, emit) => start(event, emit));
  }

  start(UserProfileEvent event, Emitter<UserProfileState> emit) async {
    try {
      if (event is InitUserProfile) {
        emit(UserProfileProgress());
        users = await DB.getUser();
        emit(InitUserProfileDone(users));
      } else if (event is SubmitUpdateProfile) {
        emit(UserProfileProgress());
        await uploading(event.user.file);
        await updateData(event.user);
        emit(ProfileUpdated(users));
      }
    } catch (e) {
      emit(UserProfileFailed(e.toString()));
    }
  }

  uploading(File? file) async {
    if (file != null) {
      String fileName = basename(file.path);
      Reference ref = FirebaseStorage.instance.ref().child(fileName);
      UploadTask uploadTask = ref.putFile(file);
      TaskSnapshot taskSnapshot = await uploadTask.whenComplete(() => null);
      await taskSnapshot.ref.getDownloadURL().then((value) => url = value);

      Uint8List bytes = await file.readAsBytes();
      await BlurHash.encode(bytes, 2, 2).then((value) => hash = value);
    }
  }

  updateData(Users result) async {
    Users data = users;
    data.userName = result.userName ?? users.userName;
    data.userProfile = result.userProfile ?? users.userProfile;
    data.userImage = url ?? users.userImage;
    data.userImageHash = hash ?? users.userImageHash;
    data.lastNameSearch = generateStringKey(data.userName);
    data.userSearch = generateStringKey(data.userName);

    await FriendQuery.updateUserData(data);
    users = await LogQuery.getUsersById(users.userId!);
    await DB.saveUser(users);
  }
}
