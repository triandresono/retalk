import 'package:retalk/models/users.dart';

class FriendProfileState {}

class InitialFriendProfile extends FriendProfileState {}

class FriendProfileOnLoading extends FriendProfileState {}

class FriendProfileLoaded extends FriendProfileState {
  final Users user;

  FriendProfileLoaded(this.user);
}

class FriendProfileFailed extends FriendProfileState {
  final String error;

  FriendProfileFailed(this.error);
}
