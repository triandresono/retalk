import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:retalk/firebase_query.dart/login_query.dart';
import 'package:retalk/models/users.dart';
import 'package:retalk/state_management/friend_profile_bloc/friend_profile_event.dart';
import 'package:retalk/state_management/friend_profile_bloc/friend_profile_state.dart';

class FriendProfileBloc extends Bloc<FriendProfileEvent, FriendProfileState> {
  Users? user;
  FriendProfileBloc() : super(InitialFriendProfile()) {
    on<FriendProfileEvent>((event, emit) => start(event, emit));
  }

  start(FriendProfileEvent event, Emitter<FriendProfileState> emit) async {
    try {
      if (event is InitFriendProfile) {
        emit(FriendProfileOnLoading());
        await initFriendProfile(event.userId);
        emit(FriendProfileLoaded(user!));
      }
    } catch (e) {
      emit(FriendProfileFailed(e.toString()));
    }
  }

  initFriendProfile(String userId) async {
    user = await LogQuery.getUsersById(userId);
  }
}
