class FriendProfileEvent {}

class InitFriendProfile extends FriendProfileEvent {
  final String userId;

  InitFriendProfile(this.userId);
}
