import 'package:retalk/models/media_model.dart';

class MediaState {}

class InitialMedia extends MediaState {}

class MediaOnLoading extends MediaState {}

class DoneSubmitMedia extends MediaState {
  final MediaModel result;

  DoneSubmitMedia(this.result);
}

class MediaFailed extends MediaState {
  final String error;

  MediaFailed(this.error);
}
