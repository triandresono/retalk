import 'dart:typed_data';

import 'package:blurhash/blurhash.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:retalk/models/media_model.dart';
import 'package:retalk/state_management/media_bloc/media_event.dart';
import 'package:retalk/state_management/media_bloc/media_state.dart';
import 'package:universal_io/io.dart';
import 'package:path/path.dart';

class MediaBloc extends Bloc<MediaEvent, MediaState> {
  String? url;
  String? hash;
  MediaModel media = MediaModel();

  MediaBloc() : super(InitialMedia()) {
    on<MediaEvent>((event, emit) => start(event, emit));
  }

  start(MediaEvent event, Emitter<MediaState> emit) async {
    try {
      if (event is SubmitMedia) {
        emit(MediaOnLoading());
        await onUpload(event.file);
        emit(DoneSubmitMedia(media));
      }
    } catch (e) {
      emit(MediaFailed(e.toString()));
    }
  }

  onUpload(File? file) async {
    if (file != null) {
      String fileName = basename(file.path);
      Reference ref = FirebaseStorage.instance.ref().child(fileName);
      UploadTask uploadTask = ref.putFile(file);
      TaskSnapshot taskSnapshot = await uploadTask.whenComplete(() => null);
      await taskSnapshot.ref.getDownloadURL().then((value) => url = value);

      Uint8List bytes = await file.readAsBytes();
      await BlurHash.encode(bytes, 2, 2).then((value) => hash = value);

      media.imageUrl = url;
      media.blurHash = hash;
      media.mediaName = fileName;
    } else {
      throw 'failed to upload image';
    }
  }
}
