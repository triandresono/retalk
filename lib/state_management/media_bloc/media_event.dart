import 'package:universal_io/io.dart';

class MediaEvent {}

class SubmitMedia extends MediaEvent {
  final File file;

  SubmitMedia(this.file);
}
