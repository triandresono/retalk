class FriendPageEvent {}

class SearchFriends extends FriendPageEvent {
  final String val;

  SearchFriends(this.val);
}

class SetChatRoom extends FriendPageEvent {
  final Map<String, dynamic> map;

  SetChatRoom(this.map);
}

class DeleteFriend extends FriendPageEvent {}

class FriendProfile extends FriendPageEvent {}
