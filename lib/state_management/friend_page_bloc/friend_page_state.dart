import 'package:cloud_firestore/cloud_firestore.dart';

class FriendPageState {}

class InitialFriendPage extends FriendPageState {}

class GetFriendProgress extends FriendPageState {}

class DeleteFriendsProgress extends FriendPageState {}

class DeleteFriendsComplete extends FriendPageState {}

class FriendsProfileComplete extends FriendPageState {}

class GetFriendDone extends FriendPageState {
  final Stream<QuerySnapshot>? result;

  GetFriendDone(this.result);
}

class SetChatRoomDone extends FriendPageState {
  final Map<String, dynamic> result;

  SetChatRoomDone(this.result);
}

class FriendPageFailed extends FriendPageState {
  final String error;

  FriendPageFailed(this.error);
}
