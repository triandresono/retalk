import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:retalk/firebase_query.dart/chat_query.dart';
import 'package:retalk/firebase_query.dart/friend_query.dart';
import 'package:retalk/firebase_query.dart/query_key.dart';
import 'package:retalk/models/users.dart';
import 'package:retalk/service.dart/database.dart';
import 'package:retalk/state_management/friend_page_bloc/friend_page_event.dart';
import 'package:retalk/state_management/friend_page_bloc/friend_page_state.dart';

class FriendPageBloc extends Bloc<FriendPageEvent, FriendPageState> {
  List<String> roomIDs = [];
  String roomId = '';
  Users user = Users();
  Stream<QuerySnapshot>? result;
  Map<String, dynamic> data = {};
  FriendPageBloc() : super(InitialFriendPage()) {
    on<FriendPageEvent>((event, emit) => start(event, emit));
  }

  start(FriendPageEvent event, Emitter<FriendPageState> emit) async {
    try {
      user = await DB.getUser();
      if (event is SearchFriends) {
        emit(GetFriendProgress());
        await getFriendList(event.val);
        emit(GetFriendDone(result));
      } else if (event is SetChatRoom) {
        emit(GetFriendProgress());
        initData(event.map);
        await updateInRoom(roomId);
        emit(SetChatRoomDone(data));
      }
    } catch (e) {
      emit(FriendPageFailed(e.toString()));
    }
  }

  getFriendList(String val) async {
    Map<String, dynamic> map = user.toMap();
    map['val'] = val;
    result = await FriendQuery.getFriendList(map);
  }

  updateInRoom(String roomId) async {
    Map<String, dynamic> map = user.toMap();
    map[RoomKey.roomId] = roomId;
    await ChatQuery.updateInRoom(map);
  }

  initData(Map<String, dynamic> map) {
    roomId = getChatRoomId(user.userId, map[UserKey.userId]);
    data[RoomKey.roomId] = roomId;
    data.addAll(map);
  }
}
