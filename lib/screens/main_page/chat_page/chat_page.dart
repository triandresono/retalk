import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:retalk/common_widgets/common_animation_list.dart';
import 'package:retalk/common_widgets/common_loading.dart';
import 'package:retalk/common_widgets/common_shimmer.dart';
import 'package:retalk/common_widgets/common_textfield.dart';
import 'package:retalk/get_it_routing/jump.dart';
import 'package:retalk/helper/page_key.dart';
import 'package:retalk/models/users.dart';
import 'package:retalk/screens/main_page/chat_page/chat_is_empty.dart';
import 'package:retalk/screens/main_page/chat_page/chat_page_tile.dart';
import 'package:retalk/state_management/chat_page_bloc/chat_page_bloc.dart';
import 'package:retalk/state_management/chat_page_bloc/chat_page_event.dart';
import 'package:retalk/state_management/chat_page_bloc/chat_page_state.dart';

class ChatPage extends StatelessWidget {
  const ChatPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ChatPageBloc>(
      create: (context) => ChatPageBloc()..add(GetChatPage('')),
      child: const ChatPageBody(),
    );
  }
}

class ChatPageBody extends StatefulWidget {
  const ChatPageBody({Key? key}) : super(key: key);

  @override
  _ChatPageBodyState createState() => _ChatPageBodyState();
}

class _ChatPageBodyState extends State<ChatPageBody> {
  TextEditingController searchCo = TextEditingController();
  Stream<QuerySnapshot>? peerList;
  Timer? timer;

  bloc(dynamic event) {
    BlocProvider.of<ChatPageBloc>(context).add(event);
  }

  @override
  void initState() {
    WidgetsBinding.instance!.addPostFrameCallback((_) {});
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<ChatPageBloc, ChatPageState>(
      listener: (context, state) {
        if (state is GetChatPageComplete) {
          setState(() {
            peerList = state.stream;
          });
        }
        if (state is GoChatRoomComplete) {
          Jump.toArg(Pages.chatRoomPage, state.result);
        }
      },
      child: SingleChildScrollView(
        child: Column(
          children: [
            const SizedBox(height: 10),
            CommonSearchField(
              hint: 'Search',
              controller: searchCo,
              onChange: (v) => onChangeHandler(v),
            ),
            StreamBuilder(
              stream: peerList,
              builder: (context, AsyncSnapshot snapshot) {
                if (snapshot.hasData) {
                  if (snapshot.data!.docs.length > 0) {
                    return ListView.builder(
                      padding: const EdgeInsets.symmetric(horizontal: 15),
                      shrinkWrap: true,
                      physics: const ScrollPhysics(),
                      itemCount: snapshot.data.docs.length,
                      itemBuilder: (context, index) {
                        DocumentSnapshot query = snapshot.data.docs[index];
                        return CommonAnimatedList(
                          index: index,
                          child: buildTile(query),
                        );
                      },
                    );
                  } else {
                    return Padding(
                      padding: const EdgeInsets.only(top: 200.0),
                      child: ChatEmpty(onTap: () => Jump.to(Pages.friendPage)),
                    );
                  }
                } else {
                  //if stream is empty
                  return const Padding(
                    padding: EdgeInsets.only(top: 200.0),
                    child: CommonLoading(),
                  );
                }
              },
            ),
          ],
        ),
      ),
    );
  }

  buildTile(DocumentSnapshot query) {
    var map = query.data() as Map<String, dynamic>;
    Users res = Users.chat(map);
    return BlocBuilder<ChatPageBloc, ChatPageState>(
      builder: (context, state) => CommonShimmer(
        isLoading: state is ChatPageOnLoading,
        child: ChatPageTile(
          chat: res,
          onTap: () => bloc(GotoChatRoom(map)),
        ),
      ),
    );
  }

  onChangeHandler(val) {
    const duration = Duration(milliseconds: 800);
    if (timer != null) setState(() => timer!.cancel());
    setState(() => timer = Timer(duration, () => onSearch(val)));
  }

  onSearch(String val) => bloc(GetChatPage(val.toLowerCase()));
}
