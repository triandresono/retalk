import 'package:flutter/material.dart';
import 'package:iconly/iconly.dart';
import 'package:retalk/common_widgets/common_avatar.dart';
import 'package:retalk/common_widgets/common_detail_animation.dart';
import 'package:retalk/helper/constant.dart';
import 'package:retalk/helper/utils.dart';
import 'package:retalk/models/users.dart';
import 'package:retalk/screens/friend_profile_page/friend_profile_page.dart';
import 'package:retalk/theme/app_theme.dart';
import 'package:retalk/theme/colors.dart';

class ChatPageTile extends StatelessWidget {
  final Users chat;
  final Function() onTap;
  const ChatPageTile({
    Key? key,
    required this.onTap,
    required this.chat,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 20),
      child: ListTile(
        onTap: onTap,
        contentPadding: const EdgeInsets.symmetric(
          vertical: 5,
          horizontal: 5,
        ),
        leading: CommonDetailAnimation(
          color: backgroundColor,
          detail: FriendProfilePage(userId: chat.userId!),
          child: CommonAvatar(
            imageUrl: chat.userImage!,
            hash: chat.userImageHash!,
          ),
        ),
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              chat.userName!,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: MyTheme.heading2.copyWith(fontSize: 16),
            ),
            const SizedBox(height: 5),
            message(),
          ],
        ),
        trailing: Column(
          children: [
            Expanded(
              child: SizedBox(
                width: 50,
                child: Material(
                  color: backgroundColor,
                  child: InkWell(
                    onTap: onTap,
                    child: const Icon(
                      IconlyBroken.more_circle,
                      color: mainColor,
                      size: 30,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  message() {
    if (chat.lastChatType == Const.typeNormal) {
      return Text(
        (chat.lastChat!.isEmpty) ? chat.userProfile! : chat.lastChat!,
        style: MyTheme.bodyText1,
        overflow: TextOverflow.ellipsis,
      );
    } else {
      return Row(
        children: [
          const Icon(IconlyLight.image_2, color: mainColor),
          const SizedBox(width: 5),
          Expanded(
            child: Text(
              (chat.lastChat!.isEmpty)
                  ? getFileName(chat.lastImage!)
                  : chat.lastChat!,
              style: MyTheme.bodyText1,
              overflow: TextOverflow.ellipsis,
            ),
          )
        ],
      );
    }
  }
}
