import 'package:flutter/material.dart';
import 'package:iconly/iconly.dart';
import 'package:retalk/theme/colors.dart';

class ChatEmpty extends StatelessWidget {
  final Function() onTap;
  const ChatEmpty({Key? key, required this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: mainColor,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(25),
          topRight: Radius.circular(25),
          bottomLeft: Radius.circular(0),
          bottomRight: Radius.circular(12),
        ),
      ),
      child: Material(
        color: mainColor,
        borderRadius: const BorderRadius.only(
          topLeft: Radius.circular(25),
          topRight: Radius.circular(25),
          bottomLeft: Radius.circular(0),
          bottomRight: Radius.circular(12),
        ),
        child: InkWell(
          onTap: onTap,
          child: Padding(
            padding: const EdgeInsets.all(15.0),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: const [
                Icon(
                  IconlyLight.chat,
                  color: backgroundColor,
                ),
                SizedBox(height: 10),
                Text(
                  'You have no chats...',
                  style: TextStyle(
                    color: backgroundColor,
                    fontWeight: FontWeight.w300,
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
