import 'package:flutter/material.dart';
import 'package:iconly/iconly.dart';
import 'package:retalk/theme/colors.dart';

class HomePageBar extends StatelessWidget {
  final Function() ontapProfile;
  const HomePageBar({Key? key, required this.ontapProfile}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Padding(
        padding: const EdgeInsets.only(left: 16, right: 16, top: 10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            const Padding(
              padding: EdgeInsets.only(top: 8.0, bottom: 8),
              child: Text(
                "RETALK",
                style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.w300,
                  color: backgroundColor,
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.all(8),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
              ),
              child: Material(
                borderRadius: BorderRadius.circular(30),
                color: mainColor,
                child: InkWell(
                  onTap: ontapProfile,
                  child: const Icon(
                    IconlyBold.profile,
                    color: backgroundColor,
                    size: 25,
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
