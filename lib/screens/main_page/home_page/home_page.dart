import 'package:flutter/material.dart';
import 'package:retalk/common_widgets/keep_alive.dart';
import 'package:retalk/get_it_routing/jump.dart';
import 'package:retalk/helper/page_key.dart';
import 'package:retalk/screens/main_page/chat_page/chat_page.dart';
import 'package:retalk/screens/main_page/home_page/home_page_bar.dart';
import 'package:retalk/theme/colors.dart';

import 'home_page_tab.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with TickerProviderStateMixin {
  late TabController tabController;
  int currentTabIndex = 0;

  @override
  void initState() {
    tabController = TabController(length: 2, vsync: this);
    tabController.addListener(onTabChange);
    super.initState();
  }

  @override
  void dispose() {
    tabController.dispose();
    super.dispose();
  }

  onTabChange() {
    currentTabIndex = tabController.index;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: mainColor,
      body: Column(
        children: [
          HomePageBar(
            ontapProfile: () => Jump.to(Pages.userProfile),
          ),
          MyTabBar(tabController),
          Expanded(
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 5),
              decoration: const BoxDecoration(
                color: backgroundColor,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(25),
                  topRight: Radius.circular(25),
                ),
              ),
              child: TabBarView(
                physics: const NeverScrollableScrollPhysics(),
                controller: tabController,
                children: const [
                  KeepAlivePage(
                    child: ChatPage(),
                  ),
                  KeepAlivePage(
                    child: Center(
                      child: Text('On Development'),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          if (currentTabIndex == 0) {
            Jump.to(Pages.friendPage);
          } else {}
        },
        backgroundColor: mainColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        child: Icon(
          currentTabIndex == 0
              ? Icons.message_outlined
              : currentTabIndex == 1
                  ? Icons.add_rounded
                  : Icons.call,
          color: backgroundColor,
        ),
      ),
    );
  }
}
