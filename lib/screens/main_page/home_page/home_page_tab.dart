import 'package:flutter/material.dart';
import 'package:retalk/theme/colors.dart';

class MyTabBar extends StatelessWidget {
  final TabController tabController;
  const MyTabBar(this.tabController, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 10, vertical: 0),
      padding: const EdgeInsets.symmetric(horizontal: 5, vertical: 20),
      height: 80,
      color: mainColor,
      child: TabBar(
        controller: tabController,
        physics: const NeverScrollableScrollPhysics(),
        isScrollable: false,
        indicatorColor: Colors.transparent,
        unselectedLabelColor: backgroundColor,
        indicator: BoxDecoration(
          color: backgroundColor,
          borderRadius: BorderRadius.circular(25),
        ),
        labelColor: mainColor,
        tabs: const [
          Tab(
            icon: Text(
              'Chats',
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w300,
              ),
            ),
          ),
          Tab(
            icon: Text(
              'Status',
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w300,
              ),
            ),
          ),
          // Tab(
          //   icon: Text(
          //     'Add friends',
          //     style: TextStyle(
          //       fontSize: 18,
          //       fontWeight: FontWeight.w300,
          //     ),
          //   ),
          // ),
        ],
      ),
    );
  }
}
