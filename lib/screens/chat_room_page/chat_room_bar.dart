import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:iconly/iconly.dart';
import 'package:retalk/common_widgets/common_avatar.dart';
import 'package:retalk/common_widgets/common_shimmer.dart';
import 'package:retalk/firebase_query.dart/query_key.dart';
import 'package:retalk/get_it_routing/jump.dart';
import 'package:retalk/helper/page_key.dart';
import 'package:retalk/models/users.dart';
import 'package:retalk/state_management/online_status_bloc/online_status_bloc.dart';
import 'package:retalk/state_management/online_status_bloc/online_status_event.dart';
import 'package:retalk/state_management/online_status_bloc/online_status_state.dart';
import 'package:retalk/theme/app_theme.dart';
import 'package:retalk/theme/colors.dart';

class ChatRoomBar extends StatefulWidget implements PreferredSizeWidget {
  final Map<String, dynamic> map;
  const ChatRoomBar({Key? key, required this.map})
      : preferredSize = const Size.fromHeight(kToolbarHeight + 5),
        super(key: key);

  @override
  final Size preferredSize; // default is 56.0

  @override
  _ChatRoomBarState createState() => _ChatRoomBarState();
}

class _ChatRoomBarState extends State<ChatRoomBar> {
  Users user = Users();
  Stream<DocumentSnapshot>? onlineStream;
  @override
  void initState() {
    user = Users.map(widget.map);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<OnlineStatusBloc>(
      create: (context) => OnlineStatusBloc()..add(InitOnline(user.userId!)),
      child: BlocListener<OnlineStatusBloc, OnlineStatusState>(
        listener: (context, state) {
          if (state is GetStatusDone) {
            setState(() {
              onlineStream = state.roomStat;
            });
          }
        },
        child: AppBar(
          elevation: 0,
          backgroundColor: mainColor,
          title: Row(
            children: [
              CommonAvatar(
                radius: 20,
                hash: user.userImageHash ?? '',
                imageUrl: user.userImage ?? '',
              ),
              const SizedBox(width: 10),
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      user.userName ?? '',
                      overflow: TextOverflow.ellipsis,
                      style: MyTheme.chatSenderName,
                    ),
                    StreamBuilder<DocumentSnapshot>(
                      stream: onlineStream,
                      builder: (context, snap) {
                        if (snap.hasData) {
                          return Text(
                            (snap.data![UserKey.onlineStatus] == true)
                                ? 'online'
                                : 'offline',
                            style: MyTheme.bodyText1.copyWith(
                              fontSize: 18,
                            ),
                          );
                        } else {
                          return CommonShimmer(
                            isLoading: true,
                            child: Text(
                              'online',
                              style: MyTheme.bodyText1.copyWith(
                                fontSize: 18,
                              ),
                            ),
                          );
                        }
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
          actions: [
            ClipRRect(
              child: InkWell(
                onTap: () {
                  var map = {UserKey.userId: user.userId};
                  Jump.toArg(Pages.friendProfile, map);
                },
                child: const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 16),
                  child: Icon(IconlyBold.profile),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
