import 'package:flutter/material.dart';
import 'package:iconly/iconly.dart';
import 'package:retalk/theme/colors.dart';

class ChatTextField extends StatelessWidget {
  final TextEditingController msgCo;
  final Function() sendTap;
  final Function() choiceTap;
  const ChatTextField({
    Key? key,
    required this.msgCo,
    required this.sendTap,
    required this.choiceTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      color: backgroundColor,
      height: 100,
      child: Row(
        children: [
          Expanded(
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 14),
              height: 60,
              decoration: BoxDecoration(
                color: mainColor.withOpacity(0.2),
                borderRadius: BorderRadius.circular(30),
              ),
              child: Row(
                children: [
                  // Icon(
                  //   Icons.emoji_emotions_outlined,
                  //   color: Colors.grey[500],
                  // ),
                  const SizedBox(width: 10),
                  Expanded(
                    child: TextField(
                      controller: msgCo,
                      onSubmitted: (v) => sendTap(),
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Type your message ...',
                        hintStyle: TextStyle(color: Colors.grey[500]),
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: choiceTap,
                    child: Icon(
                      Icons.attach_file,
                      color: Colors.grey[500],
                    ),
                  )
                ],
              ),
            ),
          ),
          const SizedBox(width: 16),
          InkWell(
            onTap: sendTap,
            child: const CircleAvatar(
              backgroundColor: mainColor,
              radius: 25,
              child: Icon(
                IconlyLight.send,
                color: Colors.white,
              ),
            ),
          )
        ],
      ),
    );
  }
}
