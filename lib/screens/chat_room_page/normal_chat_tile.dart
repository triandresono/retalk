import 'package:flutter/material.dart';
import 'package:retalk/common_widgets/common_avatar.dart';
import 'package:retalk/helper/constant.dart';
import 'package:retalk/helper/utils.dart';
import 'package:retalk/models/chat_data.dart';
import 'package:retalk/theme/app_theme.dart';
import 'package:retalk/theme/colors.dart';

class NormalChatTile extends StatelessWidget {
  final ChatData data;
  final bool isMe;
  const NormalChatTile({
    Key? key,
    required this.data,
    required this.isMe,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 10),
      child: Column(
        children: [
          Row(
            mainAxisAlignment:
                isMe ? MainAxisAlignment.end : MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              if (!isMe)
                CommonAvatar(
                  radius: 15,
                  hash: data.friendProfileHash ?? Const.emptyHash,
                  imageUrl: data.friendProfileImage ?? Const.emptyImage,
                ),
              const SizedBox(width: 10),
              Container(
                padding: const EdgeInsets.all(10),
                constraints: BoxConstraints(
                    maxWidth: MediaQuery.of(context).size.width * 0.6),
                decoration: BoxDecoration(
                    color: isMe ? mainColor : mainColor.withOpacity(0.2),
                    borderRadius: BorderRadius.only(
                      topLeft: const Radius.circular(16),
                      topRight: const Radius.circular(16),
                      bottomLeft: Radius.circular(isMe ? 12 : 0),
                      bottomRight: Radius.circular(isMe ? 0 : 12),
                    )),
                child: Text(
                  data.message!,
                  style: MyTheme.bodyTextMessage.copyWith(
                    color: isMe ? Colors.white : Colors.grey[800],
                  ),
                ),
              ),
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(top: 5),
            child: Row(
              mainAxisAlignment:
                  isMe ? MainAxisAlignment.end : MainAxisAlignment.start,
              children: [
                if (!isMe) const SizedBox(width: 40),
                isMe
                    ? Icon(
                        Icons.done_all,
                        size: 20,
                        color: !data.isRead!
                            ? MyTheme.bodyTextTime.color
                            : mainColor,
                      )
                    : const SizedBox(),
                const SizedBox(width: 8),
                Text(
                  readTimestamp(data.chatTime!),
                  style: MyTheme.bodyTextTime,
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
