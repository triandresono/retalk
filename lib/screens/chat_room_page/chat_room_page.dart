import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';
import 'package:retalk/common_widgets/common_animation_list.dart';
import 'package:retalk/common_widgets/common_detail_animation.dart';
import 'package:retalk/common_widgets/common_loading.dart';
import 'package:retalk/common_widgets/common_upload_choice.dart';
import 'package:retalk/common_widgets/custom_gallery/custom_gallery.dart';
import 'package:retalk/firebase_query.dart/chat_query.dart';
import 'package:retalk/firebase_query.dart/query_key.dart';
import 'package:retalk/helper/constant.dart';
import 'package:retalk/helper/snack_bar.dart';
import 'package:retalk/models/chat_data.dart';
import 'package:retalk/models/media_model.dart';
import 'package:retalk/models/users.dart';
import 'package:retalk/screens/chat_room_page/chat_room_bar.dart';
import 'package:retalk/screens/chat_room_page/chat_text_field.dart';
import 'package:retalk/screens/chat_room_page/detail_image.dart';
import 'package:retalk/screens/chat_room_page/image_chat_tile.dart';
import 'package:retalk/screens/chat_room_page/normal_chat_tile.dart';
import 'package:retalk/screens/media_chat_page/media_chat_page.dart';
import 'package:retalk/state_management/chat_room_bloc/chat_room_bloc.dart';
import 'package:retalk/state_management/chat_room_bloc/chat_room_event.dart';
import 'package:retalk/state_management/chat_room_bloc/chat_room_state.dart';
import 'package:retalk/state_management/permission_bloc/permission_bloc.dart';
import 'package:retalk/state_management/permission_bloc/permission_event.dart';
import 'package:retalk/state_management/permission_bloc/permission_state.dart';
import 'package:retalk/theme/colors.dart';
import 'package:universal_io/io.dart';

class ChatRoomPage extends StatelessWidget {
  final Map<String, dynamic> map;
  const ChatRoomPage({Key? key, required this.map}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<ChatRoomBloc>(
          create: (context) => ChatRoomBloc()..add(GetChatArgument()),
        ),
        BlocProvider<PermissionBloc>(
          create: (context) => PermissionBloc(),
        ),
      ],
      child: ChatRoomBody(map: map),
    );
  }
}

class ChatRoomBody extends StatefulWidget {
  final Map<String, dynamic> map;
  const ChatRoomBody({Key? key, required this.map}) : super(key: key);

  @override
  _ChatRoomBodyState createState() => _ChatRoomBodyState();
}

class _ChatRoomBodyState extends State<ChatRoomBody> {
  TextEditingController msgCo = TextEditingController();
  Stream<QuerySnapshot>? chatStream;
  Stream<DocumentSnapshot>? roomStat;
  AsyncSnapshot<dynamic>? data;
  Users user = Users();

  bloc(dynamic event) => BlocProvider.of<ChatRoomBloc>(context).add(event);

  permissionBloc(PermissionEvent event) {
    BlocProvider.of<PermissionBloc>(context).add(event);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        await updateLeave();
        return Future.value(true);
      },
      child: blocListener(
        child: Scaffold(
          backgroundColor: mainColor,
          appBar: ChatRoomBar(map: widget.map),
          body: GestureDetector(
            onTap: () => FocusScope.of(context).unfocus(),
            child: Column(
              children: [
                Expanded(
                  child: Container(
                    padding: const EdgeInsets.symmetric(horizontal: 20),
                    decoration: const BoxDecoration(
                      color: backgroundColor,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30),
                      ),
                    ),
                    child: StreamBuilder<DocumentSnapshot>(
                      stream: roomStat,
                      builder: (context, snap) {
                        if (snap.hasData) {
                          data = snap;
                          return StreamBuilder<QuerySnapshot>(
                            stream: chatStream,
                            builder: (context, snapshot2) {
                              if (snapshot2.hasData) {
                                return ListView.builder(
                                  reverse: true,
                                  itemCount: snapshot2.data!.docs.length,
                                  itemBuilder: (context, int index) {
                                    DocumentSnapshot query =
                                        snapshot2.data!.docs[index];
                                    return CommonAnimatedList(
                                      index: index,
                                      child: tile(query),
                                    );
                                  },
                                );
                              } else {
                                return const CommonLoading();
                              }
                            },
                          );
                        } else {
                          return Container();
                        }
                      },
                    ),
                  ),
                ),
                ChatTextField(
                  msgCo: msgCo,
                  sendTap: () => submit(),
                  choiceTap: () => showUploadChoice(),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget blocListener({required Widget child}) {
    return MultiBlocListener(
      child: child,
      listeners: [
        BlocListener<PermissionBloc, PermissionState>(
          listener: (context, state) {
            if (state is StoragePermissionGranted) {
              onStorage();
            }
            if (state is CameraPermissionGranted) {
              onCamera();
            }
            if (state is PermissionFailed) {
              context.fail(state.error);
            }
          },
        ),
        BlocListener<ChatRoomBloc, ChatRoomState>(
          listener: (context, state) {
            if (state is GetArgumentDone) {
              user = state.user;
              bloc(InitChatRoom(widget.map));
            }
            if (state is InitChatRoomDone) {
              chatStream = state.initChat;
              roomStat = state.roomStat;
              setState(() {});
            }

            if (state is AddChatComplete) {
              setState(() {
                msgCo.clear();
              });
            }
            if (state is ChatRoomFailed) {
              context.fail(state.message);
            }
          },
        ),
      ],
    );
  }

  tile(DocumentSnapshot query) {
    var map = query.data() as Map<String, dynamic>;
    ChatData data = ChatData.fromJson(map);
    data.friendProfileImage = widget.map[UserKey.userImage];
    data.friendProfileHash = widget.map[UserKey.userImageHash];
    data.userId = widget.map[UserKey.userId];
    if (data.chatType == Const.typeImage) {
      return CommonDetailAnimation(
        child: ImageChatTile(
          data: data,
          isMe: data.sendBy == user.userName,
        ),
        detail: DetailImage(
          user: Users.map(widget.map),
          imageHash: data.hash ?? Const.emptyHash,
          imageUrl: data.imageUrl ?? Const.emptyImage,
        ),
      );
    } else {
      return NormalChatTile(
        data: data,
        isMe: data.sendBy == user.userName,
      );
    }
  }

  submit() {
    if (msgCo.text.isNotEmpty) {
      ChatData model = ChatData();
      model.message = msgCo.text;
      model.sendBy = user.userName;
      model.chatType = Const.typeNormal;
      model.chatTime = DateTime.now().millisecondsSinceEpoch;
      model.userId = user.userId;
      model.isRead = data!.data[RoomKey.isInRoom];
      model.friendId = widget.map[UserKey.userId];
      model.roomId = widget.map[RoomKey.roomId];
      bloc(AddChatEvent(model));
    }
  }

  submitImage(MediaModel medias) {
    ChatData model = ChatData();
    model.message = medias.mediaContent;
    model.imageUrl = medias.imageUrl;
    model.hash = medias.blurHash;
    model.sendBy = user.userName;
    model.chatType = Const.typeImage;
    model.chatTime = DateTime.now().millisecondsSinceEpoch;
    model.userId = user.userId;
    model.isRead = data!.data[RoomKey.isInRoom];
    model.friendId = widget.map[UserKey.userId];
    model.roomId = widget.map[RoomKey.roomId];
    bloc(AddChatEvent(model));
  }

  updateLeave() async {
    Map<String, dynamic> map = {};
    map[RoomKey.roomId] = widget.map[RoomKey.roomId];
    map[UserKey.userId] = user.userId;
    map[RoomKey.isInRoom] = false;
    await ChatQuery.updateInRoom(map);
  }

  showUploadChoice() {
    return showModalBottomSheet(
      context: context,
      backgroundColor: backgroundColor,
      builder: (context) => SelectMediaBody(
        camera: () {
          Navigator.of(context).pop();
          permissionBloc(GetCameraPermission());
        },
        gallery: () {
          Navigator.of(context).pop();
          permissionBloc(GetStoragePermission());
        },
      ),
    );
  }

  onCamera() async {
    final ImagePicker _picker = ImagePicker();
    final XFile? photo = await _picker.pickImage(source: ImageSource.camera);
    if (photo != null) {
      Users friend = Users.map(widget.map);
      friend.file = File(photo.path);

      MediaModel? model = await Navigator.push(
        context,
        CupertinoPageRoute(
          builder: (context) => MediaChatPage(user: friend),
        ),
      );

      if (model != null) {
        submitImage(model);
      }
    }
  }

  onStorage() async {
    File? result = await Navigator.push(
      context,
      CupertinoPageRoute(
        builder: (context) => const CustomGallery(),
      ),
    );

    if (result != null) {
      Users friend = Users.map(widget.map);
      friend.file = result;
      MediaModel? model = await Navigator.push(
        context,
        CupertinoPageRoute(
          builder: (context) => MediaChatPage(user: friend),
        ),
      );

      if (model != null) {
        submitImage(model);
      }
    }
  }
}
