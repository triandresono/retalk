import 'package:flutter/material.dart';
import 'package:flutter_blurhash/flutter_blurhash.dart';
import 'package:retalk/models/users.dart';
import 'package:retalk/screens/media_chat_page/media_chat_bar.dart';

class DetailImage extends StatelessWidget {
  final Users user;
  final String imageUrl;
  final String imageHash;
  const DetailImage({
    Key? key,
    required this.user,
    required this.imageUrl,
    required this.imageHash,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black87,
      appBar: MediaChatBar(user: user),
      body: Center(
        child: SizedBox(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: BlurHash(
            hash: imageHash,
            image: imageUrl,
            imageFit: BoxFit.contain,
          ),
        ),
      ),
    );
  }
}

class DetailUserImage extends StatelessWidget {
  final String imageUrl;
  final String imageHash;
  const DetailUserImage({
    Key? key,
    required this.imageUrl,
    required this.imageHash,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      backgroundColor: Colors.black87,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: Center(
        child: SizedBox(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: BlurHash(
            hash: imageHash,
            image: imageUrl,
            imageFit: BoxFit.contain,
          ),
        ),
      ),
    );
  }
}
