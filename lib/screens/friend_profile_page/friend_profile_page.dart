import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_blurhash/flutter_blurhash.dart';
import 'package:retalk/common_widgets/common_detail_animation.dart';
import 'package:retalk/common_widgets/common_loading.dart';
import 'package:retalk/common_widgets/common_profile_text.dart';
import 'package:retalk/common_widgets/profile_bar.dart';
import 'package:retalk/helper/app_scaler.dart';
import 'package:retalk/helper/constant.dart';
import 'package:retalk/helper/snack_bar.dart';
import 'package:retalk/models/users.dart';
import 'package:retalk/screens/chat_room_page/detail_image.dart';
import 'package:retalk/screens/profile_page/common_profile_tile.dart';
import 'package:retalk/state_management/auth_bloc/auth_bloc.dart';
import 'package:retalk/state_management/friend_profile_bloc/friend_profile_bloc.dart';
import 'package:retalk/state_management/friend_profile_bloc/friend_profile_event.dart';
import 'package:retalk/state_management/friend_profile_bloc/friend_profile_state.dart';
import 'package:retalk/theme/colors.dart';

class FriendProfilePage extends StatelessWidget {
  final String userId;
  const FriendProfilePage({
    Key? key,
    required this.userId,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<FriendProfileBloc>(
      create: (context) => FriendProfileBloc()..add(InitFriendProfile(userId)),
      child: const FriendProfileBody(),
    );
  }
}

class FriendProfileBody extends StatefulWidget {
  const FriendProfileBody({Key? key}) : super(key: key);

  @override
  _FriendProfileBodyState createState() => _FriendProfileBodyState();
}

class _FriendProfileBodyState extends State<FriendProfileBody> {
  late ScrollController scrollController;
  Users user = Users();
  double scrollPosition = 0;
  double opacity = 0;

  @override
  void initState() {
    scrollController = ScrollController();
    scrollController.addListener(scrollListener);
    super.initState();
  }

  @override
  void dispose() {
    scrollController.removeListener(scrollOpacity);
    scrollController.dispose();
    super.dispose();
  }

  scrollListener() {
    setState(() {
      scrollPosition = scrollController.position.pixels;
    });
  }

  scrollOpacity() {
    opacity = scrollPosition < context.height() * 0.40
        ? scrollPosition / (context.height() * 0.40)
        : 1;
  }

  bloc(FriendProfileEvent event) {
    BlocProvider.of<FriendProfileBloc>(context).add(event);
  }

  @override
  Widget build(BuildContext context) {
    return blocListener(
      child: Scaffold(
        extendBodyBehindAppBar: true,
        backgroundColor: backgroundColor,
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(kToolbarHeight),
          child: BlocProvider<AuthBloc>(
            create: (context) => AuthBloc(),
            child: ProfileBar(
              isUser: false,
              user: user,
              opacity: opacity,
              changeImage: () {},
            ),
          ),
        ),
        body: blocBuilder(
          child: SingleChildScrollView(
            controller: scrollController,
            child: SizedBox(
              height: context.height(),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Stack(
                    children: [
                      SizedBox(
                        width: context.width(),
                        height: context.height() * 0.45,
                        child: CommonDetailAnimation(
                          detail: DetailUserImage(
                            imageHash: user.userImageHash ?? Const.emptyHash,
                            imageUrl: user.userImage ?? Const.emptyImage,
                          ),
                          child: BlurHash(
                            hash: user.userImageHash ?? Const.emptyHash,
                            image: user.userImage ?? Const.emptyImage,
                            imageFit: BoxFit.cover,
                          ),
                        ),
                      ),
                      Positioned(
                        bottom: 20,
                        left: 20,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(15),
                          child: BackdropFilter(
                            filter: ImageFilter.blur(
                              sigmaX: 10.0,
                              sigmaY: 10.0,
                            ),
                            child: Container(
                              color: mainColor.withOpacity(0.2),
                              padding: const EdgeInsets.all(5),
                              child: Text(
                                user.userName ?? '',
                                style: const TextStyle(
                                  color: projectWhite,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 30,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 20),
                  const CommonProfileText(text: 'Email'),
                  CommonProfileTile(
                    enableEdit: false,
                    text: user.userMail ?? '',
                  ),
                  const Divider(
                    height: 1,
                    color: mainColor,
                    indent: 15,
                    endIndent: 15,
                  ),
                  const SizedBox(height: 30),
                  const CommonProfileText(text: 'Username'),
                  CommonProfileTile(
                    enableEdit: false,
                    text: user.userName ?? '',
                  ),
                  const Divider(
                    height: 1,
                    color: mainColor,
                    indent: 15,
                    endIndent: 15,
                  ),
                  const SizedBox(height: 30),
                  const CommonProfileText(text: 'About Me'),
                  CommonProfileTile(
                    enableEdit: false,
                    text: user.userProfile ?? '',
                  ),
                  const Divider(
                    height: 1,
                    color: mainColor,
                    indent: 15,
                    endIndent: 15,
                  ),
                  const Spacer(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget blocListener({required Widget child}) {
    return BlocListener<FriendProfileBloc, FriendProfileState>(
      listener: (context, state) {
        if (state is FriendProfileLoaded) {
          setState(() {
            user = state.user;
          });
        }
        if (state is FriendProfileFailed) {
          context.fail(state.error);
        }
      },
      child: child,
    );
  }

  Widget blocBuilder({required Widget child}) {
    return BlocBuilder<FriendProfileBloc, FriendProfileState>(
      builder: (context, state) {
        if (state is FriendProfileOnLoading) {
          return const CommonLoading();
        } else {
          return child;
        }
      },
    );
  }
}
