import 'package:flutter/material.dart';
import 'package:retalk/firebase_query.dart/login_query.dart';
import 'package:retalk/firebase_query.dart/query_key.dart';
import 'package:retalk/models/users.dart';
import 'package:retalk/service.dart/database.dart';

class AppObserver extends StatefulWidget {
  final Widget child;
  const AppObserver(this.child, {Key? key}) : super(key: key);

  @override
  _AppObserverState createState() => _AppObserverState();
}

class _AppObserverState extends State<AppObserver> with WidgetsBindingObserver {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addObserver(this);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    super.didChangeAppLifecycleState(state);

    Users? user = await DB.getUser();
    if (user != null) {
      Map<String, dynamic> map = user.toMap();
      if (state == AppLifecycleState.resumed) {
        map[UserKey.onlineStatus] = true;
        await LogQuery.updateOnline(map);
      } else {
        map[UserKey.onlineStatus] = false;
        await LogQuery.updateOnline(map);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }
}
