import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:retalk/common_widgets/common_animation_list.dart';
import 'package:retalk/common_widgets/common_shimmer.dart';
import 'package:retalk/common_widgets/common_textfield.dart';
import 'package:retalk/get_it_routing/jump.dart';
import 'package:retalk/helper/app_scaler.dart';
import 'package:retalk/models/users.dart';
import 'package:retalk/screens/add_friend_page/add_friend_tile.dart';
import 'package:retalk/state_management/add_friend_bloc/add_friend_bloc.dart';
import 'package:retalk/state_management/add_friend_bloc/add_friend_event.dart';
import 'package:retalk/state_management/add_friend_bloc/add_friend_state.dart';
import 'package:retalk/theme/colors.dart';

class AddFriendPage extends StatelessWidget {
  const AddFriendPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<AddFriendBloc>(
      create: (context) => AddFriendBloc()..add(SearchFriendEvent('')),
      child: const AddFriendPageBody(),
    );
  }
}

class AddFriendPageBody extends StatefulWidget {
  const AddFriendPageBody({Key? key}) : super(key: key);

  @override
  _AddFriendPageBodyState createState() => _AddFriendPageBodyState();
}

class _AddFriendPageBodyState extends State<AddFriendPageBody> {
  TextEditingController searchCo = TextEditingController();
  List<DocumentSnapshot> search = [];
  Timer? timer;

  bloc(dynamic event) {
    BlocProvider.of<AddFriendBloc>(context).add(event);
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<AddFriendBloc, AddFriendState>(
      listener: (context, state) {
        if (state is SearchFriendDone) {
          setState(() {
            search = state.result;
          });
        }
        if (state is AddFriendComplete) {
          Jump.back();
        }
      },
      child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: [
            Container(
              child: (search.isNotEmpty)
                  ? ListView.builder(
                      reverse: true,
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      shrinkWrap: true,
                      physics: const ScrollPhysics(),
                      itemCount: search.length,
                      itemBuilder: (context, int index) {
                        return CommonAnimatedList(
                          index: index,
                          child: buildTile(search[index]),
                        );
                      },
                    )
                  : Container(
                      padding: const EdgeInsets.all(20),
                      child: Column(
                        children: const [
                          Icon(
                            Icons.error_outline,
                            size: 25,
                            color: mainColor,
                          ),
                          SizedBox(height: 5),
                          Text(
                            'You must know excatly your friend username',
                            style: TextStyle(
                              fontSize: 13,
                              fontWeight: FontWeight.w500,
                              color: mainColor,
                            ),
                          ),
                        ],
                      ),
                    ),
            ),
            const SizedBox(height: 10),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 5),
              child: Row(
                children: [
                  Expanded(
                    child: Container(
                      padding: const EdgeInsets.symmetric(horizontal: 5),
                      height: 50,
                      child: CommonSearchField(
                        hint: 'Search',
                        controller: searchCo,
                        onChange: (v) => onChangeHandler(v),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: context.insets() == 0 ? 10 : context.insets() + 10,
            ),
          ],
        ),
      ),
    );
  }

  buildTile(DocumentSnapshot obj) {
    var map = obj.data() as Map<String, dynamic>;
    Users user = Users.map(map);
    return BlocBuilder<AddFriendBloc, AddFriendState>(
      builder: (context, state) {
        return CommonShimmer(
          isLoading: state is AddFriendProgress,
          child: AddFriendTile(
            user: user,
            onTap: () => addFriends(user),
          ),
        );
      },
    );
  }

  onChangeHandler(val) {
    const duration = Duration(milliseconds: 800);
    if (timer != null) setState(() => timer!.cancel());
    setState(() => timer = Timer(duration, () => onSearch(val)));
  }

  onSearch(String val) => bloc(SearchFriendEvent(val.toLowerCase()));

  addFriends(Users user) => bloc(SetAddFriend(user.toMap()));
}
