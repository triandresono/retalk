import 'package:flutter/material.dart';
import 'package:iconly/iconly.dart';
import 'package:retalk/common_widgets/common_avatar.dart';
import 'package:retalk/get_it_routing/jump.dart';
import 'package:retalk/helper/constant.dart';
import 'package:retalk/models/users.dart';
import 'package:retalk/theme/colors.dart';

class MediaChatBar extends StatelessWidget implements PreferredSizeWidget {
  @override
  final Size preferredSize;
  final Users user;
  const MediaChatBar({Key? key, required this.user})
      : preferredSize = const Size.fromHeight(kToolbarHeight + 10),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 3,
      backgroundColor: mainColor,
      titleSpacing: 5,
      automaticallyImplyLeading: false,
      title: Stack(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 8.0),
            child: Row(
              children: [
                GestureDetector(
                  onTap: () => Jump.back(),
                  child: Container(
                    margin: const EdgeInsets.only(right: 15),
                    height: 40,
                    width: 40,
                    child: const Center(
                      child: Icon(IconlyLight.arrow_left),
                    ),
                  ),
                ),
                Row(
                  children: [
                    CommonAvatar(
                      imageUrl: user.userImage ?? Const.emptyImage,
                      hash: user.userImageHash ?? Const.emptyHash,
                      radius: 25,
                    ),
                    const SizedBox(width: 10),
                    Text(
                      user.userName ?? '',
                      style: const TextStyle(
                        color: backgroundColor,
                        fontWeight: FontWeight.w300,
                        fontSize: 20,
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}
