import 'package:flutter/material.dart';
import 'package:retalk/theme/colors.dart';

class MediaChatLoading extends StatelessWidget {
  const MediaChatLoading({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 15),
      color: mainColor.withOpacity(0.5),
      child: Row(
        children: [
          const SizedBox(width: 10),
          Expanded(
            child: SizedBox(
              height: 60,
              child: Center(
                child: Row(
                  children: const [
                    Center(
                      child: CircularProgressIndicator(
                        strokeWidth: 3.5,
                        valueColor: AlwaysStoppedAnimation(backgroundColor),
                      ),
                    ),
                    SizedBox(width: 10),
                    Expanded(
                      child: Text(
                        'Uploading image...',
                        style: TextStyle(color: backgroundColor),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
