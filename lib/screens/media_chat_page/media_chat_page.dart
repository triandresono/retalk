import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:retalk/helper/app_scaler.dart';
import 'package:retalk/helper/snack_bar.dart';
import 'package:retalk/models/media_model.dart';
import 'package:retalk/models/users.dart';
import 'package:retalk/screens/media_chat_page/media_chat_bar.dart';
import 'package:retalk/screens/media_chat_page/media_chat_loading.dart';
import 'package:retalk/screens/media_chat_page/media_chat_textfield.dart';
import 'package:retalk/state_management/media_bloc/media_bloc.dart';
import 'package:retalk/state_management/media_bloc/media_event.dart';
import 'package:retalk/state_management/media_bloc/media_state.dart';

class MediaChatPage extends StatelessWidget {
  final Users user;
  const MediaChatPage({Key? key, required this.user}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<MediaBloc>(
      create: (context) => MediaBloc(),
      child: MediaChatBody(user: user),
    );
  }
}

class MediaChatBody extends StatefulWidget {
  final Users user;
  const MediaChatBody({
    Key? key,
    required this.user,
  }) : super(key: key);

  @override
  _MediaChatBodyState createState() => _MediaChatBodyState();
}

class _MediaChatBodyState extends State<MediaChatBody> {
  TextEditingController msgCo = TextEditingController();
  bloc(MediaEvent event) {
    BlocProvider.of<MediaBloc>(context).add(event);
  }

  @override
  Widget build(BuildContext context) {
    return blocListener(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: Colors.black87,
        appBar: MediaChatBar(user: widget.user),
        body: Center(
          child: SizedBox(
            height: MediaQuery.of(context).size.height,
            child: Image.file(widget.user.file!),
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: blocBuilder(
          child: Padding(
            padding: EdgeInsets.only(bottom: context.insets()),
            child: MediaChatTextfield(
              msgCo: msgCo,
              sendTap: () => bloc(SubmitMedia(widget.user.file!)),
            ),
          ),
        ),
      ),
    );
  }

  Widget blocListener({required Widget child}) {
    return BlocListener<MediaBloc, MediaState>(
      listener: (context, state) {
        if (state is DoneSubmitMedia) {
          MediaModel model = state.result;
          model.mediaContent = msgCo.text;
          Navigator.of(context).pop(model);
        }
        if (state is MediaFailed) {
          context.fail(state.error);
        }
      },
      child: child,
    );
  }

  Widget blocBuilder({required Widget child}) {
    return BlocBuilder<MediaBloc, MediaState>(
      builder: (context, state) {
        if (state is MediaOnLoading) {
          return const MediaChatLoading();
        } else {
          return child;
        }
      },
    );
  }
}
