import 'package:flutter/material.dart';
import 'package:iconly/iconly.dart';
import 'package:retalk/theme/colors.dart';

class MediaChatTextfield extends StatelessWidget {
  final TextEditingController? msgCo;
  final Function()? sendTap;
  const MediaChatTextfield({
    Key? key,
    this.msgCo,
    this.sendTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 15),
      color: mainColor.withOpacity(0.5),
      child: Row(
        children: [
          Expanded(
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 14),
              height: 60,
              decoration: BoxDecoration(
                color: Colors.grey[200],
                borderRadius: BorderRadius.circular(30),
              ),
              child: Row(
                children: [
                  const SizedBox(width: 10),
                  Expanded(
                    child: TextField(
                      controller: msgCo,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintText: 'Type your message ...',
                        hintStyle: TextStyle(color: Colors.grey[500]),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          const SizedBox(width: 16),
          InkWell(
            onTap: sendTap ?? () {},
            child: const CircleAvatar(
              backgroundColor: mainColor,
              radius: 25,
              child: Icon(
                IconlyLight.send,
                color: Colors.white,
              ),
            ),
          )
        ],
      ),
    );
  }
}
