import 'package:flutter/material.dart';
import 'package:iconly/iconly.dart';
import 'package:retalk/theme/app_theme.dart';
import 'package:retalk/theme/colors.dart';

class AddFriendsTile extends StatelessWidget {
  final Function()? onTap;
  const AddFriendsTile({Key? key, this.onTap}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        margin: const EdgeInsets.only(top: 20),
        padding: const EdgeInsets.symmetric(horizontal: 20),
        color: backgroundColor,
        child: Row(
          children: [
            const CircleAvatar(
              backgroundColor: mainColor,
              radius: 28,
              child: Icon(
                IconlyBold.add_user,
                color: backgroundColor,
                size: 30,
              ),
            ),
            const SizedBox(width: 5),
            Container(
              padding: const EdgeInsets.only(left: 15, top: 10, bottom: 10),
              child: Text(
                'Add new friends',
                style: MyTheme.heading2.copyWith(
                  fontSize: 16,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
