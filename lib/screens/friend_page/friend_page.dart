import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:retalk/common_widgets/common_animation_list.dart';
import 'package:retalk/common_widgets/common_loading.dart';
import 'package:retalk/common_widgets/common_shimmer.dart';
import 'package:retalk/get_it_routing/jump.dart';
import 'package:retalk/helper/page_key.dart';
import 'package:retalk/models/users.dart';
import 'package:retalk/screens/add_friend_page/add_friend_page.dart';
import 'package:retalk/screens/friend_page/add_friend_tile.dart';
import 'package:retalk/screens/friend_page/friend_empty.dart';
import 'package:retalk/screens/friend_page/friend_page_appbar.dart';
import 'package:retalk/screens/friend_page/friend_page_tile.dart';
import 'package:retalk/state_management/friend_page_bloc/friend_page_bloc.dart';
import 'package:retalk/state_management/friend_page_bloc/friend_page_event.dart';
import 'package:retalk/state_management/friend_page_bloc/friend_page_state.dart';
import 'package:retalk/theme/colors.dart';

class FriendPage extends StatelessWidget {
  const FriendPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<FriendPageBloc>(
      create: (context) => FriendPageBloc()..add(SearchFriends('')),
      child: const FriendPageBody(),
    );
  }
}

class FriendPageBody extends StatefulWidget {
  const FriendPageBody({Key? key}) : super(key: key);

  @override
  _FriendPageBodyState createState() => _FriendPageBodyState();
}

class _FriendPageBodyState extends State<FriendPageBody> {
  Stream<QuerySnapshot>? friendList;
  Timer? timer;

  bloc(FriendPageEvent event) {
    BlocProvider.of<FriendPageBloc>(context).add(event);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBody: false,
      extendBodyBehindAppBar: false,
      backgroundColor: backgroundColor,
      appBar: FriendAppbar(onChange: (v) => onChange(v)),
      body: BlocListener<FriendPageBloc, FriendPageState>(
        listener: (context, state) {
          if (state is GetFriendDone) {
            friendList = state.result;
            setState(() {});
          }
          if (state is SetChatRoomDone) {
            Jump.toArg(Pages.chatRoomPage, state.result);
          }
        },
        child: SingleChildScrollView(
          child: Column(
            children: [
              StreamBuilder<QuerySnapshot>(
                stream: friendList,
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return Column(
                      children: [
                        (snapshot.data!.docs.isNotEmpty)
                            ? Column(
                                children: [
                                  AddFriendsTile(onTap: () => addFriends()),
                                  listTile(snapshot),
                                ],
                              )
                            : Center(
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 250.0),
                                  child: FriendEmpty(
                                    ontap: () => addFriends(),
                                  ),
                                ),
                              ),
                      ],
                    );
                  } else {
                    return const Padding(
                      padding: EdgeInsets.only(top: 200.0),
                      child: CommonLoading(),
                    );
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  listTile(dynamic snapshot) => ListView.builder(
        padding: const EdgeInsets.symmetric(
          horizontal: 20,
        ),
        shrinkWrap: true,
        physics: const ScrollPhysics(),
        itemCount: snapshot.data!.docs.length,
        itemBuilder: (context, index) {
          DocumentSnapshot query = snapshot.data!.docs[index];
          return CommonAnimatedList(
            index: index,
            child: buildTile(query),
          );
        },
      );

  buildTile(DocumentSnapshot query) {
    var map = query.data() as Map<String, dynamic>;
    Users user = Users.map(map);
    return BlocBuilder<FriendPageBloc, FriendPageState>(
      builder: (context, state) {
        return CommonShimmer(
          isLoading: state is GetFriendProgress,
          child: FriendPageTile(
            user: user,
            ontap: () => bloc(SetChatRoom(user.toMap())),
          ),
        );
      },
    );
  }

  onChange(val) {
    const duration = Duration(milliseconds: 800);
    if (timer != null) setState(() => timer!.cancel());
    setState(() => timer = Timer(duration, () => onSearch(val)));
  }

  onSearch(String val) => bloc(SearchFriends(val.toLowerCase()));

  addFriends() {
    return showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      backgroundColor: backgroundColor,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(top: Radius.circular(20)),
      ),
      builder: (context) {
        return const AddFriendPage();
      },
    );
  }
}
