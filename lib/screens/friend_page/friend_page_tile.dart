import 'package:flutter/material.dart';
import 'package:iconly/iconly.dart';
import 'package:retalk/common_widgets/common_avatar.dart';
import 'package:retalk/common_widgets/common_detail_animation.dart';
import 'package:retalk/models/users.dart';
import 'package:retalk/screens/friend_profile_page/friend_profile_page.dart';
import 'package:retalk/theme/app_theme.dart';
import 'package:retalk/theme/colors.dart';

class FriendPageTile extends StatelessWidget {
  final Users user;
  final Function() ontap;
  const FriendPageTile({
    Key? key,
    required this.user,
    required this.ontap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 20),
      child: ListTile(
        onTap: ontap,
        contentPadding: const EdgeInsets.symmetric(
          vertical: 5,
          horizontal: 5,
        ),
        leading: CommonDetailAnimation(
          detail: FriendProfilePage(userId: user.userId!),
          child: CommonAvatar(
            imageUrl: user.userImage!,
            hash: user.userImageHash!,
          ),
        ),
        title: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              user.userName!,
              maxLines: 1,
              overflow: TextOverflow.ellipsis,
              style: MyTheme.heading2.copyWith(fontSize: 16),
            ),
            const SizedBox(height: 5),
            Text(
              user.userProfile!,
              style: MyTheme.bodyText1,
            ),
          ],
        ),
        trailing: Column(
          children: [
            Expanded(
              child: SizedBox(
                width: 50,
                child: Material(
                  color: backgroundColor,
                  child: InkWell(
                    onTap: ontap,
                    child: const Icon(
                      IconlyBroken.more_circle,
                      color: mainColor,
                      size: 30,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
