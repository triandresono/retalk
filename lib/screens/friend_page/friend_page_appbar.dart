import 'package:flutter/material.dart';
import 'package:iconly/iconly.dart';
import 'package:retalk/theme/colors.dart';

class FriendAppbar extends StatefulWidget implements PreferredSizeWidget {
  final Function(String?) onChange;
  const FriendAppbar({Key? key, required this.onChange})
      : preferredSize = const Size.fromHeight(kToolbarHeight),
        super(key: key);

  @override
  final Size preferredSize; // default is 56.0

  @override
  _FriendAppbarState createState() => _FriendAppbarState();
}

class _FriendAppbarState extends State<FriendAppbar> {
  bool isSearch = false;
  FocusNode node = FocusNode();

  @override
  void dispose() {
    node.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: mainColor,
      title: Padding(
        padding: const EdgeInsets.only(top: 15.0, bottom: 15),
        child: AnimatedSwitcher(
          duration: const Duration(milliseconds: 100),
          child: (!isSearch)
              ? Container(
                  key: UniqueKey(),
                  child: const Text(
                    "Friends",
                    style: TextStyle(
                      fontSize: 22,
                      fontWeight: FontWeight.w300,
                      color: backgroundColor,
                    ),
                  ),
                )
              : TextFormField(
                  key: UniqueKey(),
                  cursorColor: Colors.white,
                  focusNode: node,
                  onChanged: (val) => widget.onChange(val),
                  style: const TextStyle(
                    color: backgroundColor,
                    fontWeight: FontWeight.w300,
                    fontSize: 22,
                  ),
                  decoration: const InputDecoration(
                    hintText: 'Search',
                    hintStyle: TextStyle(
                      color: backgroundColor,
                      fontWeight: FontWeight.w300,
                      fontSize: 22,
                    ),
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: backgroundColor),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: backgroundColor),
                    ),
                  ),
                ),
        ),
      ),
      actions: [
        ClipRRect(
          child: InkWell(
            onTap: () {
              isSearch = !isSearch;
              setState(() {});
              if (isSearch) {
                node.requestFocus();
              } else {
                node.unfocus();
              }
            },
            child: const Padding(
              padding: EdgeInsets.symmetric(horizontal: 16),
              child: Icon(IconlyLight.search),
            ),
          ),
        ),
      ],
    );
  }
}
