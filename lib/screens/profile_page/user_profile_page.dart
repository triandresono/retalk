import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_blurhash/flutter_blurhash.dart';
import 'package:image_picker/image_picker.dart';
import 'package:retalk/common_widgets/common_detail_animation.dart';
import 'package:retalk/common_widgets/common_loading.dart';
import 'package:retalk/common_widgets/common_profile_text.dart';
import 'package:retalk/common_widgets/common_upload_choice.dart';
import 'package:retalk/common_widgets/custom_gallery/custom_gallery.dart';
import 'package:retalk/helper/app_scaler.dart';
import 'package:retalk/helper/constant.dart';
import 'package:retalk/helper/snack_bar.dart';
import 'package:retalk/models/users.dart';
import 'package:retalk/screens/chat_room_page/detail_image.dart';
import 'package:retalk/screens/profile_page/common_profile_tile.dart';
import 'package:retalk/common_widgets/profile_bar.dart';
import 'package:retalk/screens/profile_page/profile_page_button.dart';
import 'package:retalk/state_management/auth_bloc/auth_bloc.dart';
import 'package:retalk/state_management/permission_bloc/permission_bloc.dart';
import 'package:retalk/state_management/permission_bloc/permission_event.dart';
import 'package:retalk/state_management/permission_bloc/permission_state.dart';
import 'package:retalk/state_management/user_profile_bloc/user_profile_bloc.dart';
import 'package:retalk/state_management/user_profile_bloc/user_profile_event.dart';
import 'package:retalk/state_management/user_profile_bloc/user_profile_state.dart';
import 'package:retalk/theme/colors.dart';
import 'package:universal_io/io.dart';

class UserProfilePage extends StatelessWidget {
  const UserProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<UserProfileBloc>(
          create: (context) => UserProfileBloc()..add(InitUserProfile()),
        ),
        BlocProvider<PermissionBloc>(
          create: (context) => PermissionBloc(),
        ),
      ],
      child: const UserProfileBody(),
    );
  }
}

class UserProfileBody extends StatefulWidget {
  const UserProfileBody({Key? key}) : super(key: key);

  @override
  _UserProfileBodyState createState() => _UserProfileBodyState();
}

class _UserProfileBodyState extends State<UserProfileBody> {
  late ScrollController scrollController;
  TextEditingController userNameCo = TextEditingController();
  TextEditingController profilCo = TextEditingController();
  FocusNode nameNode = FocusNode();
  FocusNode profileNode = FocusNode();
  String email = '';
  String userName = '';
  String profile = '';
  Users user = Users();
  bool nameEdit = false;
  bool profileEdit = false;
  double scrollPosition = 0;
  double opacity = 0;
  File? image;

  @override
  void initState() {
    scrollController = ScrollController();
    scrollController.addListener(scrollListener);
    super.initState();
  }

  scrollListener() {
    setState(() {
      scrollPosition = scrollController.position.pixels;
    });
  }

  scrollOpacity() {
    opacity = scrollPosition < context.height() * 0.40
        ? scrollPosition / (context.height() * 0.40)
        : 1;
  }

  @override
  void dispose() {
    scrollController.removeListener(scrollOpacity);
    scrollController.dispose();
    nameNode.dispose();
    profileNode.dispose();
    userNameCo.dispose();
    profilCo.dispose();
    super.dispose();
  }

  bloc(UserProfileEvent event) {
    BlocProvider.of<UserProfileBloc>(context).add(event);
  }

  permissionBloc(PermissionEvent event) {
    BlocProvider.of<PermissionBloc>(context).add(event);
  }

  @override
  Widget build(BuildContext context) {
    scrollOpacity();
    if (!nameEdit && !profileEdit) {
      scrollPosition = 0;
      scrollOpacity();
    }
    return blocListener(
      child: Scaffold(
        extendBodyBehindAppBar: true,
        backgroundColor: backgroundColor,
        appBar: PreferredSize(
          preferredSize: const Size.fromHeight(kToolbarHeight),
          child: BlocProvider<AuthBloc>(
            create: (context) => AuthBloc(),
            child: ProfileBar(
              user: user,
              opacity: opacity,
              changeImage: () => showUploadChoice(),
            ),
          ),
        ),
        body: blocBuilder(
          child: SingleChildScrollView(
            controller: scrollController,
            child: SizedBox(
              height: context.height(),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Stack(
                    children: [
                      SizedBox(
                        width: context.width(),
                        height: context.height() * 0.45,
                        child: CommonDetailAnimation(
                          detail: DetailUserImage(
                            imageHash: user.userImageHash ?? Const.emptyHash,
                            imageUrl: user.userImage ?? Const.emptyImage,
                          ),
                          child: (image != null)
                              ? Image.file(
                                  image!,
                                  fit: BoxFit.cover,
                                )
                              : BlurHash(
                                  hash: user.userImageHash ?? Const.emptyHash,
                                  image: user.userImage ?? Const.emptyImage,
                                  imageFit: BoxFit.cover,
                                ),
                        ),
                      ),
                      Positioned(
                        bottom: 20,
                        left: 20,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(15),
                          child: BackdropFilter(
                            filter: ImageFilter.blur(
                              sigmaX: 10.0,
                              sigmaY: 10.0,
                            ),
                            child: Container(
                              color: mainColor.withOpacity(0.2),
                              padding: const EdgeInsets.all(5),
                              child: Text(
                                user.userName ?? '',
                                style: const TextStyle(
                                  color: projectWhite,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 30,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 20),
                  const CommonProfileText(text: 'Email'),
                  CommonProfileTile(
                    enableEdit: false,
                    text: email,
                  ),
                  const Divider(
                    height: 1,
                    color: mainColor,
                    indent: 15,
                    endIndent: 15,
                  ),
                  const SizedBox(height: 30),
                  const CommonProfileText(text: 'Username'),
                  CommonProfileTile(
                    node: nameNode,
                    controller: userNameCo,
                    text: userName,
                    onEdit: nameEdit,
                    onTapEdit: () => onNameTap(),
                    onClosed: () => onNameTap(),
                  ),
                  const Divider(
                    height: 1,
                    color: mainColor,
                    indent: 15,
                    endIndent: 15,
                  ),
                  const SizedBox(height: 30),
                  const CommonProfileText(text: 'About Me'),
                  CommonProfileTile(
                    node: profileNode,
                    controller: profilCo,
                    text: profile,
                    onEdit: profileEdit,
                    onTapEdit: () => onProfileTap(),
                    onClosed: () => onProfileTap(),
                  ),
                  const Divider(
                    height: 1,
                    color: mainColor,
                    indent: 15,
                    endIndent: 15,
                  ),
                  const Spacer(),
                  Visibility(
                    visible: condition(),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Row(
                        children: [
                          Expanded(
                            child: ProfileButtonCancel(
                              text: 'Undo',
                              onTap: () => onUndo(),
                            ),
                          ),
                          Expanded(
                            child: ProfileButtonConfirm(
                              text: 'Save',
                              onTap: () => onSubmit(),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                  const SizedBox(height: 20),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget blocListener({required Widget child}) {
    return MultiBlocListener(
      child: child,
      listeners: [
        BlocListener<UserProfileBloc, UserProfileState>(
          listener: (context, state) {
            if (state is InitUserProfileDone) {
              insertData(state.user);
            }
            if (state is ProfileUpdated) {
              image = null;
              insertData(state.newUser);
            }
            if (state is UserProfileFailed) {
              context.fail(state.error);
            }
          },
        ),
        BlocListener<PermissionBloc, PermissionState>(
          listener: (context, state) {
            if (state is StoragePermissionGranted) {
              onStorage();
            }
            if (state is CameraPermissionGranted) {
              onCamera();
            }
            if (state is PermissionFailed) {
              context.fail(state.error);
            }
          },
        ),
      ],
    );
  }

  Widget blocBuilder({required Widget child}) {
    return BlocBuilder<UserProfileBloc, UserProfileState>(
      builder: (context, state) {
        if (state is UserProfileProgress) {
          return const CommonLoading();
        } else {
          return child;
        }
      },
    );
  }

  showUploadChoice() {
    return showModalBottomSheet(
      context: context,
      backgroundColor: backgroundColor,
      builder: (context) => SelectMediaBody(
        camera: () {
          Navigator.of(context).pop();
          permissionBloc(GetCameraPermission());
        },
        gallery: () {
          Navigator.of(context).pop();
          permissionBloc(GetStoragePermission());
        },
      ),
    );
  }

  insertData(Users data) {
    user = data;
    email = user.userMail!;
    userName = user.userName!;
    profile = user.userProfile!;
    userNameCo.text = userName;
    profilCo.text = profile;
    setState(() {});
  }

  onSubmit() {
    Users data = Users();
    data.userName = userName;
    data.userProfile = profile;
    data.file = image;
    bloc(SubmitUpdateProfile(data));
  }

  onUndo() {
    setState(() {
      userName = user.userName!;
      profile = user.userProfile!;
      userNameCo.text = userName;
      profilCo.text = profile;
      image = null;
    });
  }

  onNameTap() {
    nameEdit = !nameEdit;
    userName = userNameCo.text;
    if (profileEdit) {
      profileEdit = false;
    }
    if (nameEdit) {
      nameNode.requestFocus();
    }
    setState(() {});
  }

  onProfileTap() {
    profileEdit = !profileEdit;
    profile = profilCo.text;
    if (nameEdit) {
      nameEdit = false;
    }
    if (profileEdit) {
      profileNode.requestFocus();
    }
    setState(() {});
  }

  condition() {
    return (userName != user.userName ||
        profile != user.userProfile ||
        image != null);
  }

  onStorage() async {
    File? result = await Navigator.push(
      context,
      CupertinoPageRoute(
        builder: (context) => const CustomGallery(),
      ),
    );

    if (result != null) {
      setState(() {
        image = result;
      });
    }
  }

  onCamera() async {
    final ImagePicker _picker = ImagePicker();
    final XFile? photo = await _picker.pickImage(source: ImageSource.camera);
    if (photo != null) {
      setState(() {
        image = File(photo.path);
      });
    }
  }
}
