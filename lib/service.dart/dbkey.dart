class DBKey {
  static const String userId = 'userId';
  static const String imageUrl = 'imageUrl';
  static const String aboutMe = 'aboutMe';
  static const String email = 'email';
  static const String userName = 'userName';
  static const String friendList = 'friendList';
  static const String blockList = 'blockList';
  static const String chatRoomIDs = 'chatRoomIDs';
  static const String documentId = 'documentId';
  static const String isLoggedIn = 'isLoggedIn';
  static const String chatRooms = 'chatRooms';

  static const String user = "user";
  static const String expired = "Session Expired";
}
