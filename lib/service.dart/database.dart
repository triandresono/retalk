import 'dart:convert';

import 'package:retalk/models/users.dart';
import 'package:retalk/service.dart/dbkey.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DB {
  static saveUser(Users user) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Map loginMap = user.toMap();
    String jsonBody = json.encode(loginMap);
    await prefs.setString(DBKey.user, jsonBody);
  }

  static getUser() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Users user = Users();
    try {
      String? jsonBody = prefs.getString(DBKey.user);
      if (jsonBody != null) {
        final map = json.decode(jsonBody) as Map<String, dynamic>;
        user = Users.map(map);
      }
    } catch (e) {
      throw DBKey.expired;
    }
    return user;
  }

  static clear() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.clear();
  }

  static checkLogin() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    String? check = pref.getString(DBKey.user);
    return check != null;
  }

  static getFriends() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> friendList = prefs.getStringList(DBKey.friendList)!;
    return friendList;
  }

  static updateFriend(String? friendId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String>? friendList = prefs.getStringList(DBKey.friendList);
    friendList!.add(friendId!);
    await prefs.setStringList(DBKey.friendList, friendList);
  }

  static getChatRooms() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> chatRooms = prefs.getStringList(DBKey.chatRoomIDs)!;
    return chatRooms;
  }

  static updateChatRoomList(String chatroomId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> chatRooms = prefs.getStringList(DBKey.chatRoomIDs)!;
    chatRooms.add(chatroomId);
    await prefs.setStringList(DBKey.chatRoomIDs, chatRooms);
  }

  static initChatRoom(List<String> roomId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setStringList(DBKey.chatRoomIDs, roomId);
  }
}
