import 'package:flutter/material.dart';
import 'package:retalk/get_it_routing/locator.dart';

class Jump {
  static to(String route) {
    return locator<NavigatorService>().navigateTo(route);
  }

  static toArg(String route, dynamic obj) {
    return locator<NavigatorService>().navigateToWithArgmnt(route, obj);
  }

  static replace(String route) {
    return locator<NavigatorService>().navigateReplaceTo(route);
  }

  static back([dynamic val]) {
    return locator<NavigatorService>().goBack(val);
  }

  static BuildContext context() {
    return locator<NavigatorService>().navigatorKey.currentContext!;
  }
}
