import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:retalk/firebase_query.dart/query_key.dart';
import 'package:retalk/helper/page_key.dart';
import 'package:retalk/screens/chat_room_page/chat_room_page.dart';
import 'package:retalk/screens/friend_page/friend_page.dart';
import 'package:retalk/screens/friend_profile_page/friend_profile_page.dart';
import 'package:retalk/screens/login_page/login_main.dart';
import 'package:retalk/screens/main_page/home_page/home_page.dart';
import 'package:retalk/screens/profile_page/user_profile_page.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    try {
      switch (settings.name) {
        case Pages.loginMain:
          return CupertinoPageRoute(
            builder: (_) => const LoginMainPage(),
          );
        case Pages.homePage:
          return CupertinoPageRoute(
            builder: (_) => const HomePage(),
          );
        case Pages.friendPage:
          return CupertinoPageRoute(
            builder: (_) => const FriendPage(),
          );
        case Pages.chatRoomPage:
          var map = settings.arguments as Map<String, dynamic>;
          return CupertinoPageRoute(
            builder: (_) => ChatRoomPage(map: map),
          );
        case Pages.userProfile:
          return CupertinoPageRoute(
            builder: (_) => const UserProfilePage(),
          );
        case Pages.friendProfile:
          var map = settings.arguments as Map<String, dynamic>;
          return CupertinoPageRoute(
            builder: (_) => FriendProfilePage(
              userId: map[UserKey.userId],
            ),
          );
        default:
          return CupertinoPageRoute(
            builder: (_) => Container(),
          );
      }
    } catch (e) {
      return CupertinoPageRoute(
        builder: (_) => Container(),
      );
    }
  }
}
